// Native Javascript for Pages 4.0
(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD support:
    define([], factory);
  } else if (typeof module === 'object' && module.exports) {
    // CommonJS-like:
    module.exports = factory();
  } else {
    // Browser globals (root is window)
    var pg = factory();
    root.pg.SideBar = pg.SideBar;
    root.pg.Parallax = pg.Parallax;
    root.pg.Progress = pg.Progress;
    root.pg.MobileView = pg.MobileView;
  }
}(this, function () {

  /* Native Javascript for Pages 4.0 | Internal Utility Functions
  ----------------------------------------------------------------*/
  "use strict";
    var globalObject = typeof global !== 'undefined' ? global : this||window,
    doc = document.documentElement, body = document.body,
   // function toggle attributes
    dataToggle    = 'data-toggle',
    dataInit = 'data-pages',
    
    // components
    stringSideBar = 'SideBar',
    stringParallax = 'Parallax',
    stringMobileView = 'MobileView',
    stringQuickView = 'Quickview',
    stringProgress = 'Progress',
    stringListView = 'ListView',
    stringCard = 'Card',
  
    // event names
    clickEvent    = 'click',
    hoverEvent    = 'hover',
    keydownEvent  = 'keydown',
    resizeEvent   = 'resize',
    scrollEvent   = 'scroll',
    // originalEvents
    showEvent     = 'show',
    shownEvent    = 'shown',
    hideEvent     = 'hide',
    hiddenEvent   = 'hidden',
    closeEvent    = 'close',
    closedEvent   = 'closed',
    slidEvent     = 'slid',
    slideEvent    = 'slide',
    changeEvent   = 'change',
  
    // other
    getAttribute            = 'getAttribute',
    setAttribute            = 'setAttribute',
    hasAttribute            = 'hasAttribute',
    getElementsByTagName    = 'getElementsByTagName',
    getBoundingClientRect   = 'getBoundingClientRect',
    querySelectorAll        = 'querySelectorAll',
    getElementsByCLASSNAME  = 'getElementsByClassName',
  
    indexOf      = 'indexOf',
    parentNode   = 'parentNode',
    length       = 'length',
    toLowerCase  = 'toLowerCase',
    Transition   = 'Transition',
    Webkit       = 'Webkit',
    style        = 'style',
  
    active     = 'active',
    showClass  = 'show',
  
    // modal
    modalOverlay = 0;
    var Pages = function(){
        this.pageScrollElement = 'html, body';
        this.$body = document.getElementsByTagName('body');
        /** @function setUserOS
        * @description SET User Operating System eg: mac,windows,etc
        * @returns {string} - Appends OSName to Pages.$body
        */
        this.setUserOS = function() {
            var OSName = "";
            if (navigator.appVersion.indexOf("Win") != -1) OSName = "windows";
            if (navigator.appVersion.indexOf("Mac") != -1) OSName = "mac";
            if (navigator.appVersion.indexOf("X11") != -1) OSName = "unix";
            if (navigator.appVersion.indexOf("Linux") != -1) OSName = "linux";

            addClass(this.$body,OSName);
        };

        /** @function setUserAgent
        * @description SET User Device Name to mobile | desktop
        * @returns {string} - Appends Device to Pages.$body
        */
        this.setUserAgent = function() {
            if (navigator.userAgent.match(/Android|BlackBerry|iPhone|iPad|iPod|Opera Mini|IEMobile/i)) {
                addClass(this.$body,'mobile');
            } else {
                this.addClass(this.$body,'desktop');
                if (navigator.userAgent.match(/MSIE 9.0/)) {
                addClass(this.$body,'ie9');
                }
            }
        };

        /** @function isVisibleXs
        * @description Checks if the screen size is XS - Extra Small i.e below W480px
        * @returns {$Element} - Appends $('#pg-visible-xs') to Body
        */
        this.isVisibleXs = function() {
            var $pg_el = document.getElementById('pg-visible-xs');
            var $util_el = document.createElement('div');
            $util_el.className = "visible-xs";
            $util_el.setAttribute("id","pg-visible-xs");

            if(!$pg_el){
                this.$body[0].appendChild($util_el)
                $pg_el = document.getElementById('pg-visible-xs');
            }
            return ($pg_el.offsetWidth === 0 && $pg_el.offsetHeight === 0) ? false : true;
        };

        /** @function isVisibleSm
        * @description Checks if the screen size is SM - Small Screen i.e Above W480px
        * @returns {$Element} - Appends $('#pg-visible-sm') to Body
        */
        this.isVisibleSm = function() {
            var $pg_el = document.getElementById('pg-visible-sm');
            var $util_el = document.createElement('div');
            $util_el.className = "visible-sm";
            $util_el.setAttribute("id","pg-visible-sm");

            if(!$pg_el){
                this.$body[0].appendChild($util_el)
                $pg_el = document.getElementById('pg-visible-sm');
            }
            return ($pg_el.offsetWidth === 0 && $pg_el.offsetHeight === 0) ? false : true;
        };

        /** @function isVisibleMd
        * @description Checks if the screen size is MD - Medium Screen i.e Above W1024px
        * @returns {$Element} - Appends $('#pg-visible-md') to Body
        */
        this.isVisibleMd = function() {
            var $pg_el = document.getElementById('pg-visible-md');
            var $util_el = document.createElement('div');
            $util_el.className = "visible-md";
            $util_el.setAttribute("id","pg-visible-sm");

            if(!$pg_el){
                this.$body[0].appendChild($util_el)
                $pg_el = document.getElementById('pg-visible-md');
            }
            return ($pg_el.offsetWidth === 0 && $pg_el.offsetHeight === 0) ? false : true;
        };

        /** @function isVisibleLg
        * @description Checks if the screen size is LG - Large Screen i.e Above W1200px
        * @returns {$Element} - Appends $('#pg-visible-lg') to Body
        */
        this.isVisibleLg = function() {
            var $pg_el = document.getElementById('pg-visible-lg');
            var $util_el = document.createElement('div');
            $util_el.className = "visible-lg";
            $util_el.setAttribute("id","pg-visible-lg");

            if(!$pg_el){
                this.$body[0].appendChild($util_el)
                $pg_el = document.getElementById('pg-visible-lg');
            }
            return ($pg_el.offsetWidth === 0 && $pg_el.offsetHeight === 0) ? false : true;
        };

        /** @function getUserAgent
        * @description Get Current User Agent.
        * @returns {string} - mobile | desktop
        */
        this.getUserAgent = function() {
            return this.hasClass(document.getElementsByTagName("body"),"mobile") ? "mobile" : "desktop";
        };

        /** @function setFullScreen
        * @description Make Browser fullscreen.
        */
        this.setFullScreen = function(element) {
            // Supports most browsers and their versions.
            var requestMethod = element.requestFullScreen || element.webkitRequestFullScreen || element.mozRequestFullScreen || element.msRequestFullscreen;

            if (requestMethod) { // Native full screen.
                requestMethod.call(element);
            } else if (typeof window.ActiveXObject !== "undefined") { // Older IE.
                var wscript = new ActiveXObject("WScript.Shell");
                if (wscript !== null) {
                    wscript.SendKeys("{F11}");
                }
            }
        };

        /** @function getColor
        * @description Get Color from CSS
        * @param {string} color - pages color class eg: primary,master,master-light etc.
        * @param {int} opacity
        * @returns {rgba}
        */
        this.getColor = function(color, opacity) {
            opacity = parseFloat(opacity) || 1;

            var elem = $('.pg-colors').length ? $('.pg-colors') : $('<div class="pg-colors"></div>').appendTo('body');

            var colorElem = elem.find('[data-color="' + color + '"]').length ? elem.find('[data-color="' + color + '"]') : $('<div class="bg-' + color + '" data-color="' + color + '"></div>').appendTo(elem);

            var color = colorElem.css('background-color');

            var rgb = color.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
            var rgba = "rgba(" + rgb[1] + ", " + rgb[2] + ", " + rgb[3] + ', ' + opacity + ')';

            return rgba;
        };


        // Init DATA API
        this.initializeDataAPI = function( component, constructor, collection ){
          for (var i=0; i < collection[length]; i++) {
            new constructor(collection[i]);
          }
        };
        // class manipulation, since 2.0.0 requires polyfill.js
        this.hasClass = function(el, className) {
            return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className);
        };
        this.addClass = function(el, className) {
            if (el.classList) el.classList.add(className);
            else if (!this.hasClass(el, className)) el.className += ' ' + className;
        };
        this.removeClass = function(el, className) {
            if (el.classList) el.classList.remove(className);
            else el.className = el.className.replace(new RegExp('\\b'+ className+'\\b', 'g'), '');
        };
        this.toggleClass = function(el,className){
            if(this.hasClass(el,className)){
                this.removeClass(el,className)
            }else{
                this.addClass(el,className)
            }
        };
        this.wrap = function(el, wrapper) {
            el.parentNode.insertBefore(wrapper, el);
            wrapper.appendChild(el);
        };
        this.wrapAll = function(elms,wrapper) {
            // Cache the current parent and previous sibling of the first node.
            var parent = elms[0].parentNode;
            var previousSibling = elms[0].previousSibling;

            // Place each node in wrapper.
            //  - If nodes is an array, we must increment the index we grab from 
            //    after each loop.
            //  - If nodes is a NodeList, each node is automatically removed from 
            //    the NodeList when it is removed from its parent with appendChild.
            for (var i = 0; elms.length - i; wrapper.firstChild === elms[0] && i++) {
                wrapper.appendChild(elms[i]);
            }

            // Place the wrapper just after the cached previousSibling,
            // or if that is null, just before the first child.
            var nextSibling = previousSibling ? previousSibling.nextSibling : parent.firstChild;
            parent.insertBefore(wrapper, nextSibling);

            return wrapper;
        };
        this.addEvent = function(el, type, handler) {
            if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
        };
        this.removeEvent = function(el, type, handler) {
            if (el.detachEvent) el.detachEvent('on'+type, handler); else el.removeEventListener(type, handler);
        };
        // selection methods
        this.getElementsByClassName = function(element,classNAME) { // returns Array
          return [].slice.call(element[getElementsByCLASSNAME]( classNAME ));
        };
        this.queryElement = function (selector, parent) {
          var lookUp = parent ? parent : document;
          return typeof selector === 'object' ? selector : lookUp.querySelector(selector);
        };
        this.getClosest = function (element, selector) { //element is the element and selector is for the closest parent element to find
        // source http://gomakethings.com/climbing-up-and-down-the-dom-tree-with-vanilla-javascript/
          var firstChar = selector.charAt(0);
          for ( ; element && element !== document; element = element[parentNode] ) {// Get closest match
            if ( firstChar === '.' ) {// If selector is a class
              if ( queryElement(selector,element[parentNode]) !== null && hasClass(element,selector.replace('.','')) ) { return element; }
            } else if ( firstChar === '#' ) { // If selector is an ID
              if ( element.id === selector.substr(1) ) { return element; }
            }
          }
          return false;
        };
        this.extend = function(a, b) {
            for (var key in b) {
                if (b.hasOwnProperty(key)) {
                    a[key] = b[key];
                }
            }
            return a;
        };
        this.isTouchDevice = function(){
            return 'ontouchstart' in document.documentElement;
        }
        // event attach jQuery style / trigger  since 1.2.0
        this.on = function (element, event, handler) {
          element.addEventListener(event, handler, false);
        };
        this.off = function(element, event, handler) {
          element.removeEventListener(event, handler, false);
        };
        this.one = function (element, event, handler) { // one since 2.0.4
          on(element, event, function handlerWrapper(e){
            handler(e);
            off(element, event, handlerWrapper);
          });
        };
        this.live = function(selector, event, callback, context) {
          this.addEvent(context || document, event, function(e) {
              var found, el = e.target || e.srcElement;
              while (el && el.matches && el !== context && !(found = el.matches(selector))) el = el.parentElement;
              if (found) callback.call(el, e);
          });
        };
    }
   var pg = new Pages();
   window.pg = pg;

  // ALERT DEFINITION
  // ================
  var SideBar = function( element, options ) {
    
    // initialization element
    element = pg.queryElement(element);
    this.body =  document.body
    this.cssAnimation = true;
    this.css3d = true;
    this.sideBarWidth = 280;
    this.sideBarWidthCondensed = 280 - 70;
    this.defaults = {
        pageContainer :".page-container"
    }
    var sidebarMenu = element.querySelectorAll('.sidebar-menu > ul'),
    pageContainer = document.querySelectorAll(this.defaults.pageContainer),
    stringSideBar = "SideBar";

    var self = this,
    openSideBar = function(e){
        var _sideBarWidthCondensed = pg.hasClass(body,"rtl") ? - self.sideBarWidthCondensed : self.sideBarWidthCondensed;

         var menuOpenCSS = (this.css3d == true ? 'translate3d(' + _sideBarWidthCondensed + 'px, 0,0)' : 'translate(' + _sideBarWidthCondensed + 'px, 0)');

         if (pg.isVisibleSm() || pg.isVisibleXs()) {
             return false
         }
         // @TODO : 
         // if ($('.close-sidebar').data('clicked')) {
         //     return;
         // }
         if (pg.hasClass(self.body,"menu-pin"))
             return;

         element.style.transform = menuOpenCSS
         pg.addClass(body,'sidebar-visible');
         
    },

    closeSideBar = function(e) {
        var menuClosedCSS = (self.css3d == true ? 'translate3d(0, 0,0)' : 'translate(0, 0)');

         if (pg.isVisibleSm() || pg.isVisibleXs()) {
             return false
         }
         // @TODO : 
         // if (typeof e != 'undefined') {
         //     if (document.querySelectorAll('.page-sidebar').length) {
         //         return;
         //     }
         // }
         if (pg.hasClass(self.body,"menu-pin"))
             return;

         if (pg.hasClass(element.querySelector('.sidebar-overlay-slide'),'show')) {
            // @TODO : 
            pg.removeClass(element.querySelector('.sidebar-overlay-slide'),'show')
            // $("[data-pages-toggle']").removeClass('active')
         }
         element.style.transform = menuClosedCSS;
         pg.removeClass(self.body,'sidebar-visible');
    },
    toggleSidebar = function(toggle) {
         var timer;
         var bodyStyles = window.getComputedStyle ? getComputedStyle(body, null) : body.currentStyle;
         pageContainer[0].style.backgroundColor = bodyStyles.backgroundColor;

         if (pg.hasClass(body,'sidebar-open')) {
             pg.removeClass(body,'sidebar-open');
             timer = setTimeout(function() {
                 pg.removeClass(element,'visible');
             }.bind(this), 400);
         } else {
             clearTimeout(timer);
             pg.addClass(element,'visible');
             setTimeout(function() {
                 pg.addClass(body,'sidebar-open');
             }.bind(this), 10);
             setTimeout(function(){
                // remove background color
                pageContainer[0].style.backgroundColor = ''
             },1000);
         }
    },
    togglePinSidebar = function(toggle) {
         if (toggle == 'hide') {
             pg.removeClass(body,'menu-pin');
         } else if (toggle == 'show') {
             pg.addClass(body,'menu-pin');
         } else {
             pg.toggleClass(body,'menu-pin');
         }
    };
    // public method
    this.close = function() {
        self.closeSideBar();
    };
    this.open = function() {
        self.openSideBar();
    };
    this.menuPin = function(toggle){
        self.togglePinSidebar(toggle);
    };
    this.toggleMobileSidebar = function(toggle){

    };
    // init events 
    if ( !(this.stringSideBar in element ) ) { // prevent adding event handlers twice
      pg.on(element, "mouseenter", openSideBar);
      pg.on(pageContainer[0],'mouseover',closeSideBar)
      pg.live('.sidebar-menu a','click',function(e){
        var element = this
        if(element.parentNode.querySelectorAll(".sub-menu") === false){
            return
        }
        var parent = element.parentNode.parentNode
        var li = element.parentNode
        var sub = element.parentNode.querySelector(".sub-menu");
        if(pg.hasClass(li,"open")){
            pg.removeClass(element.querySelector(".arrow"),"open")
            pg.removeClass(element.querySelector(".arrow"),"active");
            //Velocity(sub, "stop", true);
            Velocity.animate(sub, "slideUp", { 
                duration: 200,
                complete:function(){
                    pg.removeClass(li,"open")
                    pg.removeClass(li,"active")
                } 
            });
        }
        else{
            var openMenu = parent.querySelector("li.open");
            if(openMenu){
                Velocity.animate(openMenu, "slideUp", { 
                    duration: 200,
                    complete:function(){
                        pg.removeClass(li,"open")
                        pg.removeClass(li,"active")
                        pg.removeClass(openMenu,"open");
                        pg.removeClass(openMenu,"active");
                    } 
                });
                pg.removeClass(openMenu.querySelector("li > a .arrow"),"open");
                pg.removeClass(openMenu.querySelector("li > a .arrow"),"active");
            }
            pg.addClass(element.querySelector(".arrow"),"open");
            pg.addClass(element.querySelector(".arrow"),"active");
            //Velocity(sub, "stop", true);
            Velocity.animate(sub, "slideDown", { 
                duration: 200,
                complete:function(){
                    pg.addClass(li,"open")
                    pg.addClass(li,"active")
                } 
            });                
        }
        });
    }
    pg.live('.sidebar-slide-toggle','click', function(e) {
         e.preventDefault();
         pg.toggleClass(this,'active');
         var el = this.getAttribute('data-pages-toggle');
         if (el != null) {
            //Only by ID
            el = document.getElementById(el.substr(1));
            pg.toggleClass(el,'show');
         }
    });
    element[this.stringSideBar] = this;
  };
  //TODO : Move to different Scope
  pg.initializeDataAPI(stringSideBar, SideBar, doc[querySelectorAll]('[data-pages="sidebar"]') );    

    var Parallax = function(element, options) {
        element = pg.queryElement(element);
        this.element = element;
        this.defaults = {
            speed: {
                coverPhoto: 0.3,
                content: 0.17
            },
            scrollElement: window
        }
        this.options = pg.extend(this.defaults,options)
        this.coverPhoto = element.querySelector('.cover-photo');
        // TODO: rename .inner to .page-cover-content
        this.content = element.querySelector('.inner');

        // if cover photo img is found make it a background-image
        if (this.coverPhoto) {
            var img = this.coverPhoto.querySelector('> img');
            this.coverPhoto.style.backgroundImage = 'url(' + img.getAttribute('src') + ')';
            img.parentNode.removeChild(img);
        }
        // init events 
        if ( !(this.stringParallax in element ) ) { // prevent adding event handlers twice
            if(!pg.isTouchDevice()){
                pg.on(window,scroll,this.animate())
            }
        }
        element[this.stringParallax] = this;
    }
    Parallax.prototype.animate = function() {

        var scrollPos;
        var pagecoverWidth = this.element.height;
        //opactiy to text starts at 50% scroll length
        var opacityKeyFrame = pagecoverWidth * 50 / 100;
        var direction = 'translateX';

        if (this.options.scrollElement == window){
            scrollPos = window.pageYOffset || document.documentElement.scrollTop;
        }
        else{
            scrollPos =  document.querySelector(this.options.scrollElement).scrollTop;
        }
        
        direction = 'translateY';
        var styleString = direction + '(' + scrollPos * this.options.speed.coverPhoto + 'px)';
        if (this.coverPhoto) {
            this.coverPhoto.style.transform = styleString
            //Legacy Browsers
            this.coverPhoto.style.webkitTransform = styleString
            this.coverPhoto.style.mozTransform = styleString
            this.coverPhoto.style.msTransform = styleString
        }

        this.content.style.transform = styleString
        //Legacy Browsers
        this.content.style.webkitTransform = styleString
        this.content.style.mozTransform = styleString
        this.content.style.msTransform = styleString

        if (scrollPos > opacityKeyFrame) {
            this.content.style.opacity =  1 - scrollPos / 1200;
        } else {
            this.content.style.opacity = 1;
        }

    }

    pg.initializeDataAPI(stringParallax, Parallax, doc[querySelectorAll]('[data-pages="parallax"]') );    

    //TODO : NEEDS vars than data-attributes
    var MobileView = function(element, options) {
        this.element = pg.queryElement(element);
        this.options = pg.extend(this.defaults,options);
        var self = this;

        if ( !(this.stringMobileView in element ) ) { // prevent adding event handlers twice
            pg.on(this.element,'click',function(e){
                var el = document.getElementById(this.getAttribute('data-view-port'));
                var toView = document.getElementById(this.getAttribute('data-toggle-view'));
                if (toView != null) {
                    //el.children().last().children('.view').hide();
                    toView.style.display = "block"
                }
                else{
                     toView = el.last();
                }
                pg.toggleClass(el,this.getAttribute('data-view-animation'));
                //self.options.onNavigate(toView, data.viewAnimation);
                return false;                
            })
        }
        element[this.stringSideBar] = this;
    };

    pg.initializeDataAPI(stringMobileView, MobileView, doc[querySelectorAll]('[data-navigate="view"]') );    

    var Progress = function(element, options) {
        this.element = pg.queryElement(element);
        var color = this.element.getAttribute('data-color') || null;
        var thick = this.element.getAttribute('data-thick') || false
        this.defaults = {
            value: 0,
            color : color,
            thick : thick
        }
        this.options = pg.extend(this.defaults,options);
        var self = this;
        // start adding to to DOM
        this.container = document.createElement('div');
        this.container.className = "progress-circle";

        this.options.color &&  pg.addClass(this.container,this.options.color);
        this.options.thick && pg.addClass(this.container,"progress-circle-thick");

        var pie = document.createElement('div');
        pie.className = "pie";

        var pleft = document.createElement('div');
        pleft.className = "left-side half-circle";

        var pRight = document.createElement('div');
        pRight.className = "right-side half-circle";

        var shadow = document.createElement('div');
        shadow.className = "shadow";

        pie.appendChild(pleft);
        pie.appendChild(pRight);

        this.container.appendChild(pie);
        this.container.appendChild(shadow);

        this.element.parentNode.insertBefore(this.container, this.element.nextSibling);
        // end DOM adding

        this.val = this.element.value;
        var deg = this.perc2deg(this.val);

        if (this.val <= 50) {
          pleft.style.transform = 'rotate(' + deg + 'deg)'
        } else {
          pie.style.clip = 'rect(auto, auto, auto, auto)';
          pRight.style.transform = 'rotate(180deg)';
          pleft.style.transform = 'rotate(' + deg + 'deg)';
        }
        this.bindEvents();
        element[this.stringProgress] = this;

        //Public Functions 
        this.setValue = function(val) {
          self._setValue(val)
        }
    }
    //Private Functions
    Progress.prototype ={
        perc2deg : function(p) {
            return parseInt(p / 100 * 360);
        },
        _setValue : function(e){
          var val = this.value;
          if (typeof val == 'undefined') return;

          var deg = perc2deg(val);

          if (val <= 50) {
              pleft.style.transform = 'rotate(' + deg + 'deg)'
          } else {
            pie.style.clip = 'rect(auto, auto, auto, auto)';
            pRight.style.transform = 'rotate(180deg)';
            pleft.style.transform = 'rotate(' + deg + 'deg)';
          }
        },
        bindEvents:function(){
          if ( !(this.stringProgress in this.element ) ) { // prevent adding event handlers twice
             pg.on(this.element, 'input', this._setValue);
          }
        }
    }

    pg.initializeDataAPI(stringProgress, Progress, doc[querySelectorAll]('[data-pages-progress="circle"]') );

    var Card = function(element, options) {
        this.element = pg.queryElement(element);
        this.defaults = {
            progress: 'circle',
            progressColor: 'master',
            refresh: false,
            error: null,
            overlayColor: '255,255,255',
            overlayOpacity: 0.8,
            refreshButton: '[data-toggle="refresh"]',
            maximizeButton: '[data-toggle="maximize"]',
            collapseButton: '[data-toggle="collapse"]',
            closeButton: '[data-toggle="close"]'
        }
        this.options = pg.extend(this.defaults,options);
        this.loader = null;
        this.body = this.element.querySelector('.card-block');
    }
    // Button actions
    Card.prototype = {
        collapse:function() {
          var icon = this.element.querySelector(this.options.collapseButton + ' > i');
          var heading = this.element.querySelector('.card-header');

          //TODO
          //this.$body.stop().slideToggle("fast");

          if (pg.hasClass(this.element,'card-collapsed')) {
              pg.removeClass(this.element,'card-collapsed');
              if(icon){
                icon.className = "";
                pg.addClass(icon,'pg-arrow_maximize')
              }
              //TODO
              //$.isFunction(this.options.onExpand) && this.options.onExpand(this);
              return
          }
          pg.addClass(this.element,'card-collapsed');
          if(icon){
              icon.className = "";
              pg.addClass(icon,'pg-arrow_minimize')
          }
          //TODO
          //$.isFunction(this.options.onCollapse) && this.options.onCollapse(this);
      },

      close:function() {
          this.element.parentNode.removeChild(this.element)
          //$.isFunction(this.options.onClose) && this.options.onClose(this);
      },

      maximize:function() {
          var icon = this.element.querySelector(this.options.maximizeButton + ' > i');

          if (pg.hasClass(this.element,'card-maximized')) {
              pg.removeClass(this.element,'card-maximized');
              this.element.removeAttribute('style');
              if(icon){
                pg.removeClass(icon,"pg-fullscreen_restore");
                pg.addClass(icon,'pg-arrow_maximize')
              }
              //$.isFunction(this.options.onRestore) && this.options.onRestore(this);
          } else {
              var sidebar = document.querySelector('[data-pages="sidebar"]');
              var header = document.querySelector('.header');
              var sidebarWidth = 0;
              var headerHeight = 0;
              if(sidebar){
                var rect = window.getComputedStyle(sidebar, null);
                sidebarWidth = rect.left + rect.width;
              }
              if(header){
                var rect = window.getComputedStyle(header, null);
                headerHeight = rect.height;
              }

              pg.addClass(this.element,'card-maximized');
              this.element.style.left = sidebarWidth
              this.element.style.top = headerHeight;

              if(icon){
                pg.removeClass(icon,"pg-fullscreen");
                pg.addClass(icon,'pg-fullscreen_restore');
              }
              //$.isFunction(this.options.onMaximize) && this.options.onMaximize(this);
          }
      },

      // Options
      refresh:function(refresh) {
          var toggle = this.element.querySelector(this.options.refreshButton);

          if (refresh) {
              //if (this.loader && this.$loader.is(':visible')) return;
              if (this.loader) return;
              //if (!$.isFunction(this.options.onRefresh)) return; // onRefresh() not set
              this.loader = document.createElement('div');
              this.loader.style.backgroundColor = 'rgba(' + this.options.overlayColor + ',' + this.options.overlayOpacity + ')'

              var elem = document.createElement('div');

              if (this.options.progress == 'circle') {
                  elem.className = "progress progress-small progress-circle-"+ this.options.progressColor;
              } else if (this.options.progress == 'bar') {
                  elem.className = "progress progress-small";
                  var child = document.createElement("div");
                  child.className = "progress-bar-indeterminate progress-bar-"+ this.options.progressColor;
                  elem.appendChild(child);
              } else if (this.options.progress == 'circle-lg') {
                  pg.addClass(toggle,'refreshing');
                  var iconOld = toggle.querySelector('> i');
                  var iconNew;
                  if (!toggle.find('[class$="-animated"]').length) {
                      iconNew = document.createElement("i");
                      iconNew.style.position = "absolute";
                      iconNew.style.top = "absolute";
                      iconNew.style.left = "absolute";

                      var rect = window.getComputedStyle(iconOld, null);
                      iconNew.css({
                          'position': 'absolute',
                          'top': rect.top+"px",
                          'left': rect.left+"px"
                      });
                      pg.addClass(iconNew,'card-icon-refresh-lg-' + this.options.progressColor + '-animated');
                      toggle.appendChild(iconNew);
                  } else {
                      iconNew = toggle.querySelector('[class$="-animated"]');
                  }

                  pg.addClass(iconOld,'fade');
                  pg.addClass(iconNew,'active');


              } else {
                  elem.className = "progress progress-small";
                  var child = document.createElement("div");
                  child.className = "progress-bar-indeterminate progress-bar-" + this.options.progressColor;
                  elem.appendChild(child);
              }

              this.loader.appendChild(elem);
              this.element.appendChild(this.$loader);

              // Start Fix for FF: pre-loading animated to SVGs
              var _loader = this.$loader;
              setTimeout(function() {
                  this.loader.parentNode.removeChild(this.loader)
                  this.element.appendChild(_loader);
              }.bind(this), 300);
              // End fix
              //TODO :
              //this.$loader.fadeIn();

              //$.isFunction(this.options.onRefresh) && this.options.onRefresh(this);

          } else {
              //TODO :
              // var _this = this;
              // this.$loader.fadeOut(function() {
              //     $(this).remove();
              //     if (_this.options.progress == 'circle-lg') {
              //         var iconNew = toggle.find('.active');
              //         var iconOld = toggle.find('.fade');
              //         iconNew.removeClass('active');
              //         iconOld.removeClass('fade');
              //         toggle.removeClass('refreshing');

              //     }
              //     _this.options.refresh = false;
              // });
          }
      },

      error:function(error) {
          if (error) {
              var _this = this;

              //TODO
              // this.$element.pgNotification({
              //     style: 'bar',
              //     message: error,
              //     position: 'top',
              //     timeout: 0,
              //     type: 'danger',
              //     onShown: function() {
              //         _this.$loader.find('> div').fadeOut()
              //     },
              //     onClosed: function() {
              //         _this.refresh(false)
              //     }
              // }).show();
          }
      }
    }

    // SEARCH CLASS DEFINITION
    // ======================

    var Search = function(element, options) {
        this.element = pg.queryElement(element);
        this.defaults = {
          searchField: '[data-search="searchField"]',
          closeButton: '[data-search="closeButton"]',
          suggestions: '[data-search="suggestions"]',
          brand: '[data-search="brand"]'
        }
        this.options = pg.extend(this.defaults,options);
        this.init();
    }
    //Search.VERSION = "1.0.0";

    Search.prototype = {
        init:function() {
          var _this = this;
          this.pressedKeys = [];
          this.ignoredKeys = [];

          //Cache elements
          this.searchField = this.element.querySelector(this.options.searchField);
          this.closeButton = this.element.querySelector(this.options.closeButton);
          this.suggestions = this.element.querySelector(this.options.suggestions);
          this.brand = this.element.querySelector(this.options.brand);

          pg.on(this.searchField,'keyup', function(e) {
              _this.suggestions && _this.$suggestions.innerHTML(this.value);
          });

          pg.on(this.searchField,'keyup', function(e) {
              _this.options.onKeyEnter && _this.options.onKeyEnter(_this.searchField.value);
              if (e.keyCode == 13) { //Enter pressed
                  e.preventDefault();
                  _this.options.onSearchSubmit && _this.options.onSearchSubmit(_this.searchField.value);
              }
              if (pg.hasClass(document.body,'overlay-disabled')) {
                  return 0;
              }

          });

          pg.on(this.closeButton,'click', function() {
              _this.toggleOverlay('hide');
          });

          pg.on(this.element,'click', function(e) {
              // if ($(e.target).data('pages') == 'search') {
              //     _this.toggleOverlay('hide');
              // }
          });

          // $(document).on('keypress.pg.search', function(e) {
          //     _this.keypress(e);
          // });

          // $(document).on('keyup', function(e) {
          //     // Dismiss overlay on ESC is pressed
          //     if (_this.$element.is(':visible') && e.keyCode == 27) {
          //         _this.toggleOverlay('hide');
          //     }
          // });

      },


      keypress:function(e) {

          e = e || event; // to deal with IE
          var nodeName = e.target.nodeName;
          if (pg.hasClass(document.body,'overlay-disabled') ||
              pg.hasClass(e.target,'js-input') ||
              nodeName == 'INPUT' ||
              nodeName == 'TEXTAREA') {
              return;
          }

          if (e.which !== 0 && e.charCode !== 0 && !e.ctrlKey && !e.metaKey && !e.altKey && e.keyCode != 27) {
              this.toggleOverlay('show', String.fromCharCode(e.keyCode | e.charCode));
          }
      },


      toggleOverlay:function(action, key) {
          var _this = this;
          if (action == 'show') {
              pg.removeClass(this.element,"hide");
              //this.$element.fadeIn("fast");
              if (!this.searchField.hasFocus()) {
                  this.searchField.value = key;
                  setTimeout(function() {
                      this.searchField.focus();
                      var tmpStr = this.searchField.value;
                      this.searchField.value = "";
                      this.searchField.value = tmpStr;
                  }.bind(this), 10);
              }

              pg.removeClass(this.element,"closed");
              this.brand && pg.toggleClass(this.brand,'invisible');
              //$(document).off('keypress.pg.search');
          } else {
              //this.$element.fadeOut("fast").addClass("closed");
              this.searchField.val = ""
              this.searchField.blur();
              setTimeout(function() {
                  // if ((this.$element).is(':visible')) {
                  //     this.$brand.toggleClass('invisible');
                  // }
                  // $(document).on('keypress.pg.search', function(e) {
                  //     _this.keypress(e);
                  // });
              }.bind(this), 10);
          }
      }
    };

    var ListView = function(elem, options) {
        this.elem = pg.queryElement(elem);
        this.elem[this.stringListView] = this;
        this.init(options);
    };

    ListView.prototype = {
        defaults: {
            classes: {
                animated: "list-view-animated",
                container: "list-view-wrapper",
                hidden: "list-view-hidden",
                stationaryHeader: "list-view-fake-header"
            },
            selectors: {
                groupContainer: ".list-view-group-container",
                groupHeader: ".list-view-group-header",
                stationaryHeader: "h2"
            }
        },

        init: function(options) {
            var scope = this,
                isIOS = navigator.userAgent.match(/ipad|iphone|ipod/gi) ? true : false;

            //set defaults
            this.options = pg.extend(this.defaults,options);
            this.elems = [];
            //indicate that this is an ioslist
            pg.addClass(this.elem,'ioslist');
            //wrap all the children
            var wrapper = document.createElement('div');
            wrapper.className = this.options.classes.container;
            wrapper.setAttribute("data-ios",isIOS);

            pg.wrapAll(this.elem.childNodes,wrapper)

            var newEl = document.createElement(this.options.selectors.stationaryHeader);
            this.elem.insertBefore(newEl,this.elem.childNodes[0]);

            this.listWrapper = this.elem.querySelector('.' + this.options.classes.container);
            this.fakeHeader = this.elem.querySelector(this.options.selectors.stationaryHeader);
            pg.addClass(this.fakeHeader,this.options.classes.stationaryHeader);

            this.refreshElements();

            this.fakeHeader.innerHTML = this.elems[0].headerText;
            if ( !("ListView" in this.elem ) ) { // prevent adding event handlers twice
              if(this.listWrapper.addEventListener)
                this.listWrapper.addEventListener('scroll', function(){
                  scope.testPosition()
                }, false);   
              else if (this.listWrapper.attachEvent)
                this.listWrapper.attachEvent('onscroll', function(){
                  scope.testPosition()  
                }); 
            }

        },

        refreshElements: function() {
            var scope = this;
            this.elems = [];
            var groupContainers = this.elem.querySelectorAll(this.options.selectors.groupContainer);
            [].forEach.call(groupContainers, function(el) {
                var tmp_header = el.querySelector(scope.options.selectors.groupHeader),
                    tmp_wrapper_rect = scope.listWrapper.getBoundingClientRect(),
                    tmp_rect  = el.getBoundingClientRect(),
                    tmp_styles = window.getComputedStyle(tmp_header, null);

                scope.elems.push({
                    'list': el,
                    'header': tmp_header,
                    'listHeight': tmp_rect.height,
                    'headerText': tmp_header.innerHTML,
                    'headerHeight': tmp_header.getBoundingClientRect().height + parseFloat(tmp_styles.marginTop) + parseFloat(tmp_styles.marginBottom),
                    'listOffset': tmp_rect.top - tmp_wrapper_rect.top,
                    'listBottom': tmp_rect.height + (tmp_rect.top - tmp_wrapper_rect.top)
                });
            });
        },

        testPosition: function() {
            var currentTop = this.listWrapper.scrollTop,
                topElement, offscreenElement, topElementBottom, i = 0;

            while ((this.elems[i].listOffset - currentTop) <= 0) {
                topElement = this.elems[i];
                topElementBottom = topElement.listBottom - currentTop;
                if (topElementBottom < -topElement.headerHeight) {
                    offscreenElement = topElement;
                }
                i++;
                if (i >= this.elems.length) {
                    break;
                }
            }

            if (topElementBottom < 0 && topElementBottom > -topElement.headerHeight) {
                pg.addClass(this.fakeHeader,this.options.classes.hidden);
                pg.addClass(topElement.list,this.options.classes.animated)
            } else {
                pg.removeClass(this.fakeHeader,this.options.classes.hidden);
                if (topElement) {
                    pg.removeClass(topElement.list,this.options.classes.animated);
                }
            }

            if (topElement) {
                this.fakeHeader.innerHTML = topElement.headerText;
            }
        }
    };
    pg.initializeDataAPI(stringListView, ListView, doc[querySelectorAll]('[data-pages="list-view"]') );


  return {
    SideBar : SideBar
  };
}));
