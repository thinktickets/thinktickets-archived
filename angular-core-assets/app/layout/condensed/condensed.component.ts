import { Component, OnInit, OnDestroy, ViewChild, HostListener, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-condensed',
  templateUrl: './condensed.component.html',
  styleUrls: ['./condensed.component.scss']
})
export class CondensedComponent implements OnInit {
  @ViewChild('root') root;
  constructor() { }

  ngOnInit() {
  }

}
