import { Component, OnInit,ElementRef } from '@angular/core';
declare var pg: any;
declare var Tooltip: any;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  host: {'class': 'page-sidebar','data-pages':'sidebar'}
})
export class SidebarComponent implements OnInit {

  constructor(private appSidebar: ElementRef) { 
  	
  }

  ngOnInit() {
  	new pg.SideBar(this.appSidebar.nativeElement)
  }

}
