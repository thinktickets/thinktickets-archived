import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar/sidebar.component';
import { QuickviewComponent } from './quickview/quickview.component';
import { SearchOverlayComponent } from './search-overlay/search-overlay.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [SidebarComponent, QuickviewComponent, SearchOverlayComponent]
})
export class SharedModule { }
