import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppRoutes } from './app.routing';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppComponent } from './app.component';
//Layouts
import { CondensedComponent } from './layout/condensed/condensed.component';

//Shared Components
import { SidebarComponent } from './layout/shared/sidebar/sidebar.component';
import { QuickviewComponent } from './layout/shared/quickview/quickview.component';
import { SearchOverlayComponent } from './layout/shared/search-overlay/search-overlay.component';

@NgModule({
  declarations: [
    AppComponent,
    CondensedComponent,
    SidebarComponent,
    QuickviewComponent,
    SearchOverlayComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(AppRoutes),
    NgbModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
