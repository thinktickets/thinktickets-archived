import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankpageComponent } from './blankpage/blankpage.component';
const routes: Routes = [
  {
    path: '',
    children: [{
      path: 'blank',
      component: BlankpageComponent
    }]
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExtraRoutingModule { }
