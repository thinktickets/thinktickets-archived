import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ExtraRoutingModule } from './extra-routing.module';
import { BlankpageComponent } from './blankpage/blankpage.component';

@NgModule({
  imports: [
    CommonModule,
    ExtraRoutingModule
  ],
  declarations: [BlankpageComponent]
})
export class ExtraModule { }
