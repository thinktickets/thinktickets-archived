import { Routes } from '@angular/router';

import { CondensedComponent } from './layout/condensed/condensed.component';

export const AppRoutes: Routes = [
  {
    path: '',
    component: CondensedComponent,
        children: [
        {
          path: 'extra',
          loadChildren: './extra/extra.module#ExtraModule'
        }]
  }
];
