
Pages Third Party Plugins
=====================

Global
---------

Plugin 										| 		Version
---------									|		--------
Bootstrap 								| 		3.3.4
Bootstrap Select2					|		4.0.0.3 
Classie 									| 		-
jQuery										|		1.11.1
jQuery Actual							|		1.0.16
jQuery Bez								|		-
jQuery Easy								|		-
jQuery iOSList							|		2.0.0 
jQuery Scrollbar						|		0.2.8
jQuery UI									|		1.11.1
jQuery Unveil							|		-
Pace											|		1.0.2
Modernizr								|		-
Switchery									|		-

Widgets
-----------
Plugin 										| 		Version
---------									|		--------
DialogFx									|		-
Images Loaded							|		3.1.8	
Isotope										|		2.0.0
MetroJs	 								| 		-
Moment 									| 		2.8.3

Email
--------
Plugin 										| 		Version
---------									|		--------
Bootstrap3 WYSIHTML5			|		0.3.0
jQuery Menuclipper					|		-


Social
-----------
Plugin 										| 		Version
---------									|		--------
Images Loaded							|		3.1.8	
Isotope										|		2.0.0
Steps Form								| 		-

Calendar
--------------------
Plugin 										| 		Version
---------									|		--------
Interact									|		1.2.6
Moment 									| 		2.8.3

UI Elements- Icons
--------------------------
Plugin 										| 		Version
---------									|		--------
jQuery Sieve								|		0.3.0

UI Elements- Tabs and Accordion
----------------------------------------------
Plugin 										| 		Version
---------									|		--------
Bootstrap Tabcollapse				|		-

UI Elements- Sliders
-----------------------------
Plugin 										| 		Version
---------									|		--------
Ion RangeSlider						|		2.0.1
jQuery Liblink							| 		-
jQuery Nouislider						|		-

UI Elements- Tree View
-----------------------------
Plugin 										| 		Version
---------									|		--------
jQuery Dynatree 						|		1.2.6

UI Elements- Nestable
-----------------------------
Plugin 										| 		Version
---------									|		--------
jQuery Nestable						|		-

Forms - Form Elements
-----------------------------
Plugin 										| 		Version
---------									|		--------
Bootstrap Date Picker				|		-
Bootstrap Date Range Picker	|		-
Bootstrap Time Picker				|		0.2.6
Bootstrap Tags Input 				|		0.3.7
Bootstrap3 WYSIHTML5			|		0.3.0
Dropzone									|		-
Handlebars								|		4.0.5
jQuery AutoNumeric				| 		-
jQuery Bootstrap Wizard			|		1.0
jQuery Masked Input				|		1.3.1
jQuery Validation						|		1.13.0
Moment									|		2.8.3
Summernote							|		-
Typeahead								|		0.11.1

Forms - Form Layouts
-------------------------------
Plugin 										| 		Version
---------									|		--------
Bootstrap Date Picker				|		-
jQuery Validation						|		1.13.0

Forms - Form Wizard
------------------------------
Plugin 										| 		Version
---------									|		--------
Bootstrap Date Picker				|		-
Bootstrap Date Range Picker	|		-
Bootstrap Time Picker				|		0.2.6
Bootstrap Tags Input 				|		0.3.7
Bootstrap3 WYSIHTML5			|		0.3.0
Dropzone									|		-
jQuery AutoNumeric				| 		-
jQuery Bootstrap Wizard			|		1.0
jQuery Masked Input				|		1.3.1
jQuery Validation						|		1.13.0
Moment									|		2.8.3
Summernote							|		-

Tables- Basic Tables
----------------------------
Plugin 										| 		Version
---------									|		--------
DataTables Bootstrap				|		-
DataTables TableTools				| 		2.2.4
Datatables Responsive			|		-
jQuery DataTables					|		1.10.9
jQuery Datatable Bootstrap		|		-
jQuery TableTools					| 		2.2.4
Lo-Dash									| 		1.3.1

Tables- Data Tables
----------------------------
Plugin 										| 		Version
---------									|		--------
DataTables Bootstrap				|		-
DataTables TableTools				| 		2.2.4
Datatables Responsive			|		-
jQuery DataTables					|		1.10.9
jQuery Datatable Bootstrap		|		-
Lo-Dash									| 		1.3.1

Maps- Vector Maps
---------------------------
Plugin 										| 		Version
---------									|		--------
Hammer									|		-
jQuery Mousewheel					|		-
Mapplic									|		-

Charts
---------
Plugin 										| 		Version
---------									|		--------
Axis											|		-
Bootstrap Date Picker				|		-
D3 V3										|		-
Hammer									|		-
Interactive Layer						|		-
jQuery Mousewheel					|		-
jQuery Sparkline						|		-
Line											|		-
Line With Focus Chart				|		-
Mapplic									|		-
MetroJs									|		-
NV D3										|		-
Rickshaw									|		-
Skycons									|		-
Tooltip										|		-
Utils											|		-

Extra- Login
-----------------
Plugin 										| 		Version
---------									|		--------
jQuery Validation						|		1.13.0

Extra- Register
---------------------
Plugin 										| 		Version
---------									|		--------
jQuery Validation						|		1.13.0

Extra- Lock Screen
--------------------------
Plugin 										| 		Version
---------									|		--------
jQuery Validation						|		1.13.0

Extra- Gallery
-------------------
Plugin 										| 		Version
---------									|		--------
DialogFx									|		-
Images Loaded							|		3.1.8
Isotope										|		2.0.0
jQuery Liblink							|		-
jQuery Nouislider						|		-
MetroJs									|		-
Owl Carousel 							|		-



