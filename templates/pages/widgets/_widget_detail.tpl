    <!-- START DIALOG -->
    <div id="widgetDetails" class="dialog item-details">
      <div class="dialog__overlay">
        <div class="progress-circle-indeterminate"> </div>
        
      </div>
      <div class="dialog__content full-height">
        <button class="close action top-right" data-dialog-close><i class="pg-close"></i>
        </button>
        <div class="container-fluid full-height">
          <div class="row dialog__overview full-height">
            <div class="col-sm-12 no-padding item-slideshow-wrapper full-height">
              <div class="item-slideshow full-height">
                <iframe id="widget-preview" class="no-border" style="height:100%"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="item-description p-t-20 p-b-20">
        <div class="container">

          <div class="col-sm-4">
            <h4 class="no-margin light" id="widget-title"></h4>
          </div>

          <div class="col-sm-3">
            <p class="fs-13" id="widget-description">
            </p>
          </div>
          <div class="col-sm-2">
            <div class=""><span class="hint-text fs-12">Added on:</span>
            </div>
            <div class=""><span id="widget-date"></span></div>
          </div>
          <div class="col-sm-3">
            <div id="widget-tags"></div>
          </div>
        

        </div>
      </div>
    </div>
    <!-- END DIALOG -->