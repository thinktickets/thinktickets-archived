<div class="card no-border no-margin widget-loader-bar">
    <div class="container-xs-height full-height">
        <div class="row-xs-height">
            <div class="col-xs-height col-top">
                <div class="card-header   top-left top-right">
                    <div class="card-title">
                        <span class="font-montserrat fs-11 all-caps">Weekly Sales <i class="fa fa-chevron-right"></i>
                        </span>
                    </div>
                    <div class="card-controls">
                        <ul>
                            <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i class="card-icon card-icon-refresh"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row-xs-height">
            <div class="col-xs-height col-top">
                <div class="p-l-20 p-t-50 p-b-40 p-r-20">
                    <h1 class="no-margin p-b-5">$23,000</h1>
                    <span class="small hint-text pull-left">71% of total goal</span>
                    <span class="pull-right small text-primary">$30,000</span>
                </div>
            </div>
        </div>
        <div class="row-xs-height">
            <div class="col-xs-height col-bottom">
                <div class="progress progress-small m-b-0">
                    <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                    <div class="progress-bar progress-bar-primary" style="width:45%"></div>
                    <!-- END BOOTSTRAP PROGRESS -->
                </div>
            </div>
        </div>
    </div>
</div>