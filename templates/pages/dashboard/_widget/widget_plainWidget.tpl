
	<div class="card no-border bg-master widget widget-6 widget-loader-circle-lg no-margin">
		<div class="card-header ">
			<div class="card-controls">
				<ul>
					<li><a data-toggle="refresh" class="card-refresh" href="#"><i class="card-icon card-icon-refresh-lg-white"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="card-body">
			<div class="pull-bottom bottom-left bottom-right padding-25">
				<h1 class="text-white semi-bold">30&#176;</h1>
				<span class="label font-montserrat fs-11">TODAY</span>
				<p class="text-white m-t-20">Feels like rainy</p>
				<p class="text-white hint-text m-t-30">November 2014, 5 Thusday </p>
			</div>
		</div>
	</div>
