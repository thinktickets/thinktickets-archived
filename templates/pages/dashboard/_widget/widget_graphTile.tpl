
	<div class="widget-4 card no-border  no-margin widget-loader-bar">
		<div class="container-sm-height full-height d-flex flex-column">
			<div class="card-header  ">
				<div class="card-title text-black hint-text">
                          <span class="font-montserrat fs-11 all-caps">
                              Product revenue <i class="fa fa-chevron-right"></i>
                          </span>
				</div>
				<div class="card-controls">
					<ul>
						<li><a href="#" class="card-refresh text-black"
						       data-toggle="refresh"><i
								class="card-icon card-icon-refresh"></i></a>
						</li>
					</ul>
				</div>
			</div>
			<div class="p-l-20 p-r-20">
				<h5 class="no-margin p-b-5 pull-left hint-text">Pages</h5>

				<p class="pull-right no-margin bold hint-text">2,563</p>

				<div class="clearfix"></div>
			</div>
			<div class="widget-4-chart line-chart mt-auto" data-line-color="success"
		     data-area-color="success-light" data-y-grid="false" data-points="false"
		     data-stroke-width="2">
			<svg></svg>
			</div>
		</div>
	</div>