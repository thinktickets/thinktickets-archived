<div class="card">
	<div class="card-body no-padding">
	  <div class="p-t-40 p-b-20">
	  <!-- BEGIN Comment -->
	  <div class="row p-l-20 p-r-20 p-b-30">
	    <!-- BEGIN Profile Picture -->
	    <div class="thumbnail-wrapper d32 circular b-white m-r-10 b-a b-white">
	      <img width="30" height="30" data-src-retina="{# ASSETS #}/img/profiles/1x.jpg" data-src="{# ASSETS #}/img/profiles/1.jpg" alt="" src="{# ASSETS #}/img/profiles/1.jpg">
	    </div>
	    <!-- BEGIN END Picture -->
	    <!-- BEGIN Comment text -->
	    <div class="col-sm-10 col-xs-12 no-padding">
	      <!-- BEGIN Username -->
	      <span class="text-black">Ellan Grants</span>
	      <!-- END Username -->
	      <!-- BEGIN Comment -->
	      <p>Theres way too much things happening 
	      inside sledom too pleople..</p>
	      <!-- BEGIN Controls -->
	      <div class="clearfix">
	        <div class="pull-left small hint-text">2 mins ago</div>
	        <div class="pull-right fs-12">
	          <a href="#" class="small hint-text text-black m-r-5">Like</a>
	          <a href="#" class="small hint-text text-black">Reply</a>
	        </div>
	      </div>
	      <!-- END Controls -->
	    </div>
	    <!-- END Comment text -->
	  </div>
	   <!-- END Comment -->

	  <!-- BEGIN Comment -->
	  <div class="row p-l-20 p-r-20">
	    <!-- BEGIN Profile Picture -->
	    <div class="thumbnail-wrapper d32 circular b-white m-r-10 sm-m-b-5 b-a b-white">
	      <img width="30" height="30" data-src-retina="{# ASSETS #}/img/profiles/2x.jpg" data-src="{# ASSETS #}/img/profiles/2.jpg" alt="" src="{# ASSETS #}/img/profiles/2.jpg">
	    </div>
	    <!-- BEGIN END Picture -->
	    <!-- BEGIN Comment text -->
	    <div class="col-sm-10 col-xs-12 no-padding">
	      <!-- BEGIN Username -->
	      <span class="text-black">Carlos Roberts</span>
	      <!-- END Username -->
	      <!-- BEGIN Comment -->
	      <p>Theres way too much things happening 
	      inside sledom too pleople..</p>
	      <!-- BEGIN Controls -->
	      <div class="clearfix">
	        <div class="pull-left small hint-text">2 mins ago</div>
	        <div class="pull-right fs-12">
	          <a href="#" class="small hint-text text-black m-r-5">Like</a>
	          <a href="#" class="small hint-text text-black">Reply</a>
	        </div>
	      </div>
	        <!-- BEGIN Comment -->
	        <div class="row p-b-30 p-t-20 sm-p-l-15">
	          <!-- BEGIN Profile Picture -->
	          <div class="thumbnail-wrapper d24 circular b-white m-r-10 sm-m-b-5 b-a b-white">
	            <img width="24" height="24" data-src-retina="{# ASSETS #}/img/profiles/3x.jpg" data-src="{# ASSETS #}/img/profiles/3.jpg" alt="" src="{# ASSETS #}/img/profiles/3.jpg">
	          </div>
	          <!-- BEGIN END Picture -->
	          <!-- BEGIN Comment text -->
	          <div class="col-sm-10 col-xs-12 no-padding">
	            <!-- BEGIN Username -->
	            <span class="text-black">Carlos Roberts</span>
	            <!-- END Username -->
	            <!-- BEGIN Comment -->
	            <p>Check out more of our work at <br>
	            <a href="#">www.revox.io</a></p>
	            <!-- BEGIN Controls -->
	            <div class="clearfix">
	              <div class="pull-left small hint-text text-black">2 mins ago</div>
	            </div>
	            <!-- END Controls -->
	          </div>
	          <!-- END Comment text -->
	        </div>
	         <!-- END Comment -->
	      <!-- END Controls -->
	    </div>
	    <!-- END Comment text -->
	  </div>
	   <!-- END Comment -->

	  <div class="row b-t b-grey p-t-20 p-l-20 p-r-20">
	    <!-- BEGIN Comment -->
	      <!-- BEGIN Profile Picture -->
	      <div class="thumbnail-wrapper d32 circular b-white m-r-10  b-a b-white">
	        <img width="30" height="30" data-src-retina="{# ASSETS #}/img/profiles/avatar_small2x.jpg" data-src="{# ASSETS #}/img/profiles/avatar.jpg" alt="" src="{# ASSETS #}/img/profiles/avatar.jpg">
	      </div>
	      <!-- BEGIN END Picture -->
	      <!-- BEGIN Comment text -->
	      <div class="col-sm-10 no-padding">
	        <!-- BEGIN Username -->
	        <span class="text-black">David Nester</span>
	        <!-- END Username -->
	        <!-- BEGIN Comment -->
	        <div class="input-group transparent no-border full-width">
	          <textarea rows="2" placeholder="Write a comment.." class="no-border fs-14 p-l-0 p-t-5 p-b-10 full-width"></textarea>
	        </div>
	        <!-- BEGIN Controls -->
	        <div class="clearfix">
	          <div class="pull-right">
	            <a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12">Comment</a>
	          </div>
	        </div>
	        <!-- END Controls -->
	      </div>
	      <!-- END Comment text -->
	     <!-- END Comment -->
	  </div>

	</div>
	</div>