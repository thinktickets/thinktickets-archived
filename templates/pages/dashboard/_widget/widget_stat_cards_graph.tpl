<div class="card card-default hover-fill">
    <div class="card-header ">
    </div>


    <div class="card-body no-padding">
        <div class="p-l-15">
            <div class="row no-margin">
                <div class="col-md-8 p-b-20">
                    <p class="fs-16 text-black">Pages - Admin Dashboard beyond
                    what Imagined Experience the revolution</p>


                    <p class="small hint-text">VIA pages- dashboard revo<br>
                    Fri, Jul 25, 2014 4:00 PM EDT</p>
                </div>
            </div>
        </div>


        <div class="col-md-4 no-padding col-md-offset-8">
            <div class="pull-bottom">
                <div class="with-transitions line-chart" id=
                "pages-visits-chart-mini" style="height:100px;">
                </div>
            </div>
        </div>
    </div>
</div>

<br>

<div class="card card-default e">
    <div class="card-header ">
    </div>


    <div class="card-body no-padding">
        <div class="p-l-15 p-b-20">
            <div class="row no-margin">
                <div class="col-md-8">
                    <p class="fs-16 text-black">Pages - Admin Dashboard beyond
                    what Imagined Experience the revolution</p>


                    <p class="small hint-text">VIA pages- dashboard revo<br>
                    Fri, Jul 25, 2014 4:00 PM EDT</p>
                </div>


                <div class="col-md-4">
                    <h5 class="font-montserrat bold text-success">+0.94</h5>


                    <h5 class="font-montserrat bold text-danger">-0.63</h5>
                </div>
            </div>
        </div>
    </div>
</div>