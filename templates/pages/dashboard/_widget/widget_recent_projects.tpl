<div class=" card no-border  no-margin widget-loader-circle todolist-widget">
    <div class="card-header ">
        <div class="card-title">
            <span class="font-montserrat fs-11 all-caps">
          Recent projects <i class="fa fa-chevron-right"></i>
      </span>
        </div>
        <div class="card-controls">
            <ul>
                <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
class="card-icon card-icon-refresh"></i></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="card-body">
            <h5 class="">Ongoing projects at <span class="semi-bold">pages</span></h5>
        <ul class="nav nav-tabs nav-tabs-simple" role="tablist" data-init-reponsive-tabs="collapse">
            <li class="active"><a href="#pending" data-toggle="tab" role="tab" aria-expanded="true">Pending</a>
            </li>
            <li class=""><a href="#completed" data-toggle="tab" role="tab" aria-expanded="false">Completed</a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="pending">
                <div class="p-t-25">
                    <div class="">
                        <span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
                        <div class="pull-left">
                            <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Revox Ltd
                            </p>
                            <h5 class="no-margin overflow-ellipsis ">Marketing Campaign for revox</h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-25">
                        <p class="hint-text fade small pull-left">71% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="progress progress-small m-b-20 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-info" style="width:71%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                </div>
                <div class="p-t-25">
                    <div class="">
                        <span class="icon-thumbnail bg-warning-light pull-left text-master">cr</span>
                        <div class="pull-left">
                            <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Nike Ltd
                            </p>
                            <h5 class="no-margin overflow-ellipsis ">Corporate rebranding</h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-25">
                        <p class="hint-text fade small pull-left">20% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="progress progress-small m-b-20 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-warning" style="width:20%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                </div>
                <a href="#" class="btn btn-block m-t-30">See all projects</a>
            </div>
            <div class="tab-pane" id="completed">
                <div class="p-t-25">
                    <div class="">
                        <span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
                        <div class="pull-left">
                            <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Apple Corp
                            </p>
                            <h5 class="no-margin overflow-ellipsis ">Marketing Campaign for revox</h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-25">
                        <p class="hint-text fade small pull-left">45% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="progress progress-small m-b-20 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-info" style="width:45%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                </div>
                <div class="p-t-25">
                    <div class="">
                        <span class="icon-thumbnail bg-warning-light pull-left text-master">cr</span>
                        <div class="pull-left">
                            <p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Yahoo Inc
                            </p>
                            <h5 class="no-margin overflow-ellipsis ">Corporate rebranding</h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="m-t-25">
                        <p class="hint-text fade small pull-left">20% compleated from total</p>
                        <a href="#" class="pull-right text-master"><i class="pg-more"></i></a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="progress progress-small m-b-20 m-t-10">
                        <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
                        <div class="progress-bar progress-bar-warning" style="width:20%"></div>
                        <!-- END BOOTSTRAP PROGRESS -->
                    </div>
                </div>
                <a href="#" class="btn btn-block m-t-30">See all projects</a>
            </div>
        </div>
    </div>
</div>