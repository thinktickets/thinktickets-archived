 <div class="card card-default hover-fill p-t-20">
    <div class="card-body no-padding no-overflow">
        <div class="p-l-15">
            <div class="row">
                <div class="col-xs-8">
                    <div class="row no-margin">
                        <div class="p-b-20">
                            <p class="fs-16 text-black">Pages - Admin Dashboard beyond what Imagined Experience the revolution</p>
                            <p class="small hint-text">VIA pages- dashboard revo
                                <br> Fri, Jul 25, 2014 4:00 PM EDT</p>
                        </div>
                    </div>
                </div>
                <div class="col-xs-4 no-padding" style="height:100px">
                    <div class="pull-bottom">
                        <div class='widget-8-chart line-chart' data-line-color="primary" data-points="true" data-point-color="primary" data-stroke-width="2">
                            <svg></svg>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card card-default p-t-20">
    <div class="card-body no-padding">
        <div class="p-l-15 p-b-20">
            <div class="row no-margin">
                <div class="col-xs-8">
                    <p class="fs-16 text-black">Pages - Admin Dashboard beyond what Imagined Experience the revolution</p>
                    <p class="small hint-text">VIA pages- dashboard revo
                        <br> Fri, Jul 25, 2014 4:00 PM EDT</p>
                </div>
                <div class="col-xs-4">
                    <h5 class="font-montserrat bold text-success">+0.94</h5>
                    <h5 class="font-montserrat bold text-danger">-0.63</h5>
                </div>
            </div>
        </div>
    </div>
</div>