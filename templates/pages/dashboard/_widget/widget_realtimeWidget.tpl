	<div class="widget-14 card no-border  no-margin widget-loader-circle">
		<div class="container-xs-height full-height">
			<div class="row-xs-height">
				<div class="col-xs-height">
					<div class="card-header ">
						<div class="card-title">Server load
						</div>
						<div class="card-controls">
							<ul>
								<li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
										class="card-icon card-icon-refresh"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="row-xs-height">
				<div class="col-xs-height">
					<div class="p-l-20 p-r-20">
						<p>Server: www.revox.io</p>

						<div class="row">
							<div class="col-lg-3 col-md-12">
								<h4 class="bold no-margin">5.2GB</h4>

								<p class="small no-margin">Total usage</p>
							</div>
							<div class="col-lg-3 col-md-6">
								<h5 class=" no-margin p-t-5">227.34KB</h5>

								<p class="small no-margin">Currently</p>
							</div>
							<div class="col-lg-3 col-md-6">
								<h5 class=" no-margin p-t-5">117.65MB</h5>

								<p class="small no-margin">Average</p>
							</div>
							<div class="col-lg-3 visible-xlg">
								<div class="widget-14-chart-legend bg-transparent text-black no-padding pull-right"></div>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row-xs-height">
				<div class="col-xs-height relative bg-master-lightest">
					<div class="widget-14-chart_y_axis"></div>
					<div class="widget-14-chart rickshaw-chart top-left top-right bottom-left bottom-right"></div>
				</div>
			</div>
		</div>
	</div>