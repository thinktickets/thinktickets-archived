
	<div class="widget-16 card no-border  no-margin widget-loader-circle">
		<div class="card-header ">
			<div class="card-title">Page Options
			</div>
			<div class="card-controls">
				<ul>
					<li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
							class="card-icon card-icon-refresh"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="widget-16-header padding-20 d-flex">
			<span class="icon-thumbnail bg-master-light pull-left text-master">ws</span>
			<div class="flex-1 full-width overflow-ellipsis">
				<p class="hint-text all-caps font-montserrat  small no-margin overflow-ellipsis ">Pages
					name</p>
				<h5 class="no-margin overflow-ellipsis ">Webarch Sales Analysis</h5>
			</div>
		</div>
		<div class="p-l-25 p-r-45 p-t-25 p-b-25">
			<div class="row">
				<div class="col-md-4 ">
					<p class="hint-text all-caps font-montserrat small no-margin ">Views</p>

					<p class="all-caps font-montserrat  no-margin text-success ">14,256</p>
				</div>
				<div class="col-md-4 text-center">
					<p class="hint-text all-caps font-montserrat small no-margin ">Today</p>

					<p class="all-caps font-montserrat  no-margin text-warning ">24</p>
				</div>
				<div class="col-md-4 text-right">
					<p class="hint-text all-caps font-montserrat small no-margin ">Week</p>

					<p class="all-caps font-montserrat  no-margin text-success ">56</p>
				</div>
			</div>
		</div>
		<div class="relative no-overflow">
			<div class="widget-16-chart line-chart" data-line-color="success" data-points="true"
			     data-point-color="white" data-stroke-width="2">
				<svg></svg>
			</div>
		</div>
		<div class="b-b b-t b-grey p-l-20 p-r-20 p-b-10 p-t-10">
			<p class="pull-left">Post is Public</p>

			<div class="pull-right">
				<input type="checkbox" data-init-plugin="switchery"/>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="b-b b-grey p-l-20 p-r-20 p-b-10 p-t-10">
			<p class="pull-left">Maintenance mode</p>

			<div class="pull-right">
				<input type="checkbox" data-init-plugin="switchery" checked="checked"/>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="p-l-20 p-r-20 p-t-10 p-b-10 ">
			<p class="pull-left no-margin hint-text">Super secret options</p>
			<a href="#" class="pull-right"><i class="fa fa-arrow-circle-o-down text-success fs-16"></i></a>

			<div class="clearfix"></div>
		</div>
	</div>