<div class="widget-pie-chart card card-condensed  no-margin no-border widget-loader-circle">
    <div class="card-header ">
        <div class="card-controls">
            <ul>
                <li>
                    <a href="#" class="card-refresh text-black" data-toggle="refresh">
                        <i class="card-icon card-icon-refresh"></i>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="p-l-30 p-r-30">
        <h2 class="text-complete no-margin inline bold">753</h2>
        <p class="all-caps font-montserrat  small no-margin overflow-ellipsis inline m-l-20">Comments
        </p>
        <p class="hint-text small">From overall activities</p>
    </div>
    <div class="card-body">
        <div id="nvd3-pie" class="text-center m-b-30">
            <svg style="width:300px;height:400px; margin:0 auto"></svg>
        </div>
    </div>
</div>