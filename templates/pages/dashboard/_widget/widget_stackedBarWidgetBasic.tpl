
	<div class="widget-15-2 card no-margin no-border widget-loader-circle">
		<div class="card-header  top-right">
			<div class="card-controls">
				<ul>
					<li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
							class="card-icon card-icon-refresh"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<ul class="nav nav-tabs nav-tabs-simple">
			<li>
				<a href="#widget-15-2-tab-1" class="active">
					APPL<br>
					22.23<br>
					<span class="text-success">+60.223%</span>
				</a>
			</li>
			<li><a href="#widget-15-2-tab-2">
				FB<br>
				45.97<br>
				<span class="text-danger">-06.56%</span>
			</a>
			</li>
			<li><a href="#widget-15-2-tab-3">
				GOOG<br>
				22.23<br>
				<span class="text-success">+60.223%</span>
			</a>
			</li>
		</ul>
		<div class="tab-content">
			<div class="tab-pane no-padding active" id="widget-15-2-tab-1">
				<div class="full-width">
					<div class="widget-15-chart2 rickshaw-chart full-height"></div>
				</div>
			</div>
			<div class="tab-pane no-padding" id="widget-15-2-tab-2">
			</div>
			<div class="tab-pane" id="widget-15-2-tab-3">
			</div>
		</div>
		<div class="p-t-10 p-l-20 p-r-20 p-b-30">
			<div class="row">
				<div class="col-md-9">
					<p class="fs-16 text-black">Apple’s Motivation - Innovation distinguishes between A
						leader and a follower.
					</p>

					<p class="small hint-text">VIA Apple Store (Consumer and Education Individuals)
						<br>(800) MY-APPLE (800-692-7753)
					</p>
				</div>
				<div class="col-md-3 text-right">
					<h5 class="font-montserrat bold text-success">+0.94</h5>
					<h5 class="font-montserrat bold text-danger">-0.63</h5>
				</div>
			</div>
		</div>
	</div>