<div class="card pending-comments-widget">
	<div class="widget-header p-r-20 p-l-20">
		<!-- BEGIN Widget title -->
		<div class="card-title">
			<h5 class="text-uppercase font-montserrat m-t-0 p-t-20 p-l-10">Pending comments</h5>
		</div>
		<!-- END Widget title -->

		<!-- BEGIN Select element -->
		<div class="row">
			<div class="col-sm-6 col-xs-12 p-l-10 col-md-8">
			  <select data-init-plugin="select2" class="full-width">
			    <option>www.revox.io/pages</option>
			    <option>www.revox.io/dashboard</option>
			  </select>
			</div>
		</div>
		<!-- END Select element -->

		<!-- BEGIN Tabs -->
		<ul class="nav nav-tabs nav-tabs-simple font-montserrat text-uppercase">
			<li class="nav-item"><a href="#" class="active" data-toggle="tab" role="tab">Pending</a></li>
			<li class="nav-item"><a href="#" class="" data-toggle="tab" role="tab">Approved</a></li>
		</ul>
		<!-- END Tabs -->

		<!-- BEGIN Refresh button -->
		<div class="card-header  top-right">
			<div class="card-controls">
				<ul>
					<li><a href="#" class="card-refresh" data-toggle="refresh"><i class="card-icon card-icon-refresh text-white"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<!-- END Refresh button -->
	</div>
	<div class="tab-content card-body no-padding scrollable">
		<div class="p-t-35 p-b-20 scroll-content">
			<!-- BEGIN Comment -->
			<div class="d-flex p-l-25 p-r-25 p-b-30">
				<!-- BEGIN Profile Picture -->
				<div class="thumbnail-wrapper d32 circular b-white m-r-10 b-a b-white">
					<img width="30" height="30" data-src-retina="{# ASSETS #}/img/profiles/1x.jpg" data-src="{# ASSETS #}/img/profiles/1.jpg" alt="" src="{# ASSETS #}/img/profiles/1.jpg">
				</div>
				<!-- BEGIN END Picture -->

				<!-- BEGIN Comment text -->
				<div class="flex-1 comment-wrapper pull-left no-padding">
					<!-- BEGIN Username -->
					<span class="comment-username text-black m-r-10">Ellan Grants</span>
					<!-- END Username -->

					<!-- BEGIN Comment source -->
					<span class="hint-text fs-11">on Pages.io</span>
					<!-- END Comment source -->

					<!-- BEGIN More icon -->
					<a href="#" class="text-master m-l-10"><i class="pg-more"></i></a>
					<!-- END More icon -->

					<!-- BEGIN Comment timestamp -->
					<div class="pull-right small hint-text text-black">2 mins ago</div>
					<!-- END Comment timestamp -->

					<!-- BEGIN Comment -->
					<p class="hint-text">Theres way too much things happening 
					inside sledom too pleople..</p>
					<!-- END Comment -->

					<!-- BEGIN Controls -->
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-r-10 comment-button">Approve</a>
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-l-10 m-r-15 comment-button">Reject</a>
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-l-15 m-r-15 comment-button view-button">View</a>
					<!-- END Controls -->
				</div>
				<!-- END Comment text -->
			</div>
			<!-- END Comment -->

			<!-- BEGIN Comment -->
			<div class="d-flex p-l-25 p-r-25 p-b-30">
				<!-- BEGIN Profile Picture -->
				<div class="thumbnail-wrapper d32 circular b-white m-r-10 sm-m-b-5 b-a b-white">
					<img width="30" height="30" data-src-retina="{# ASSETS #}/img/profiles/2x.jpg" data-src="{# ASSETS #}/img/profiles/2.jpg" alt="" src="{# ASSETS #}/img/profiles/2.jpg">
				</div>
				<!-- BEGIN END Picture -->

				<!-- BEGIN Comment text -->
				<div class="flex-1 comment-wrapper pull-left no-padding">
					<!-- BEGIN Username -->
					<span class="comment-username text-black m-r-10">Carlos Roberts</span>
					<!-- END Username -->

					<!-- BEGIN Comment source -->
					<span class="hint-text fs-11">on Pages.io</span>
					<!-- END Comment source -->

					<!-- BEGIN More icon -->
					<a href="#" class="text-master m-l-10"><i class="pg-more"></i></a>
					<!-- END More icon -->

					<!-- BEGIN Comment timestamp -->
					<div class="pull-right small hint-text text-black">2 mins ago</div>
					<!-- END Comment timestamp -->

					<!-- BEGIN Comment -->
					<p class="hint-text">Theres way too much things happening 
					inside sledom too pleople..</p>
					<!-- END Comment -->

					<!-- BEGIN Controls -->
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-r-10 comment-button">Approve</a>
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-l-10 m-r-15 comment-button">Reject</a>
					<a href="#" class="btn btn-link font-montserrat text-uppercase text-complete fs-12 p-l-0 p-r-0 m-l-15 m-r-15 comment-button view-button">View</a>
					<!-- END Controls -->
				</div>
				<!-- END Comment text -->
			</div>
			<!-- END Comment -->
		</div>
	</div>
</div>