<div class=" card no-border  no-margin widget-loader-circle todolist-widget full-height">
    <div class="card-header ">
        <div class="card-title">TODOLIST
        </div>
        <div class="card-controls">
            <ul>
                <li><a href="#" class="card-refresh text-black" data-toggle="refresh"><i
                        class="card-icon card-icon-refresh"></i></a>
                </li>
            </ul>
        </div>
    </div>

    <ul class="list-unstyled p-l-25 p-r-25 p-t-10 m-b-20">
        <li>
            <h5 class="pull-left normal no-margin">28th September</h5>
            <a href="#" class="text-black pull-right m-l-5" data-toggle="refresh"><i class="fa fa-angle-right"></i></a>
            <a href="#" class="text-black pull-right m-r-5" data-toggle="refresh"><i class="fa fa-angle-left"></i></a>

        </li>
        <div class="clearfix"></div>
    </ul>
    <div class="p-t-0 p-r-20 p-b-20 p-l-20 clearfix flex-1">
            <!-- START TAKS !-->
            <div class="task clearfix row completed">
                <div class="task-list-title col-sm-10 justify-content-between">
                    <a href="#" class="text-master strikethrough" data-task="name">Purchase Pages before 10am
                    </a>
                    <i class="fs-14 pg-close hidden"></i>
                </div>
                <div class="checkbox checkbox-circle no-margin text-center col-sm-2">
                    <input type="checkbox" checked="checked" value="1" id="todocheckbox1" data-toggler="task" class="hidden">
                    <label for="todocheckbox1" class=" no-margin no-padding"></label>
                </div>
            </div>
            <!-- END TAKS !-->
            <!-- START TAKS !-->
            <div class="task clearfix row">
                <div class="task-list-title col-sm-10 justify-content-between">
                    <a href="#" class="text-master strikethrough" data-task="name">Meeting with CFO
                    </a>
                    <i class="fs-14 pg-close hidden"></i>
                </div>
                <div class="checkbox checkbox-circle no-margin text-center col-sm-2">
                    <input type="checkbox" checked="checked" value="1" id="todocheckbox2" data-toggler="task" class="hidden">
                    <label for="todocheckbox2" class=" no-margin no-padding"></label>
                </div>
            </div>
            <!-- END TAKS !-->
            <!-- START TAKS !-->
            <div class="task clearfix row">
                <div class="task-list-title col-sm-10 justify-content-between">
                    <a href="#" class="text-master strikethrough" data-task="name">AGM Conference at 1pm
                    </a>
                    <i class="fs-14 pg-close hidden"></i>
                </div>
                <div class="checkbox checkbox-circle no-margin text-center col-sm-2">
                    <input type="checkbox" checked="checked" value="1" id="todocheckbox3" data-toggler="task" class="hidden">
                    <label for="todocheckbox3" class=" no-margin no-padding"></label>
                </div>
            </div>
            <!-- END TAKS !-->
            <!-- START TAKS !-->
            <div class="task clearfix row">
                <div class="task-list-title col-sm-10 justify-content-between">
                    <a href="#" class="text-master strikethrough" data-task="name">Revise Annual Reports
                    </a>
                    <i class="fs-14 pg-close hidden"></i>
                </div>
                <div class="checkbox checkbox-circle no-margin text-center col-sm-2">
                    <input type="checkbox" checked="checked" value="1" id="todocheckbox4" data-toggler="task" class="hidden">
                    <label for="todocheckbox4" class=" no-margin no-padding"></label>
                </div>
            </div>
            <!-- END TAKS !-->
    </div>
    <div class="clearfix"></div>
    <div class="bg-master-light padding-20 full-width ">
      <div class="row">
      <div class="col-xs-10">
        <p class="no-margin normal text-black">Type Event Here</p>
        <div class="input-group transparent no-border full-width">
          <input class="form-control transparent p-l-0" type="text" placeholder="What do you need to remeber?">
        </div>
      </div>
      <div class="col-xs-2 text-center">
        <a href="#" class="block m-t-15"><img src="{# ASSETS #}/img/plus.svg"></a>
      </div>
    </div>
    </div>
 </div>
