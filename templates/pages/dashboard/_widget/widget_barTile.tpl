
	<div class="widget-5 card no-border  widget-loader-bar">
		<div class="card-header  pull-top top-right">
			<div class="card-controls">
				<ul>
					<li><a data-toggle="refresh" class="card-refresh text-black" href="#"><i
							class="card-icon card-icon-refresh"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="container-xs-height full-height">
			<div class="row row-xs-height">
				<div class="col-xs-5 col-xs-height col-middle relative">
					<div class="padding-15 top-left bottom-left">
						<h5 class="hint-text no-margin p-l-10">France, Florence</h5>

						<p class=" bold font-montserrat p-l-10">2,345,789
							<br>USD</p>

						<p class=" hint-text visible-xlg p-l-10">Today's sales</p>
					</div>
				</div>
				<div class="col-xs-7 col-xs-height col-bottom relative widget-5-chart-container">
					<div class="widget-5-chart"></div>
				</div>
			</div>
		</div>
	</div>