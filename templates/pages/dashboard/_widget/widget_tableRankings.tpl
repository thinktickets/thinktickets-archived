<div class="card no-border card-default no-margin widget-loader-circle">
    <div class="card-header  p-l-30 p-t-30 p-r-30">
    	<div class="pull-left">
    		<div class="card-title">Pages Ranking
        	</div>
        	<div class="">
                <img src="img/revox-logo.png">
                <span class="hint-text fs-12">pages.revox.io</span>
            </div>
    	</div>
        <div class="input-prepend input-group pull-right col-sm-4">
            <input type="text" style="width: 100%" name="reservation" id="rankingDatepicker" class="form-control" value="08/01/2013 1:00 PM - 08/01/2013 1:30 PM" />
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="p-l-30 p-r-30 p-b-30">
        <table class="table table-hover " id="rankingTable">
            <thead>
                <tr>
                    <th class="hint-text fs-11 col-xs-5">Keyword</th>
                    <th class="hint-text fs-11">Avg Search volume</th>
                    <th class="hint-text fs-11">Current google rank</th>
                    <th class="hint-text fs-11">Rank change</th>
                    <th class="hint-text fs-11">Traffic change</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>748</p>
                    </td>
                    <td class="v-align-middle">
                        <p>481</p>
                    </td>
                    <td class="v-align-middle">
                        <p>4 <i class="pg-arrow_up text-success"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>1</p>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>888</p>
                    </td>
                    <td class="v-align-middle">
                        <p>321</p>
                    </td>
                    <td class="v-align-middle">
                        <p>2 <i class="pg-arrow_down text-danger"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>1</p>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>116</p>
                    </td>
                    <td class="v-align-middle">
                        <p>878</p>
                    </td>
                    <td class="v-align-middle">
                        <p>2 <i class="pg-arrow_up text-success"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>0</p>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>748</p>
                    </td>
                    <td class="v-align-middle">
                        <p>78</p>
                    </td>
                    <td class="v-align-middle">
                        <p>5 <i class="pg-arrow_down text-danger"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>1</p>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>651</p>
                    </td>
                    <td class="v-align-middle">
                        <p>351</p>
                    </td>
                    <td class="v-align-middle">
                        <p>3 <i class="pg-arrow_up text-success"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>5</p>
                    </td>
                </tr>
                <tr>
                    <td class="v-align-middle">
                        <p>Paint it the way you Like it.</p>
                    </td>
                    <td class="v-align-middle">
                        <p>119</p>
                    </td>
                    <td class="v-align-middle">
                        <p>84</p>
                    </td>
                    <td class="v-align-middle">
                        <p>6 <i class="pg-arrow_down text-danger"></i></p>
                    </td>
                    <td class="v-align-middle">
                        <p>0</p>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</div>