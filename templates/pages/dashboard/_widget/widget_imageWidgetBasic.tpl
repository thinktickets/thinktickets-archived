
	<div class="widget-2 card no-border bg-primary widget widget-loader-circle-lg no-margin">
		<div class="card-header ">
			<div class="card-controls">
				<ul>
					<li><a href="#" class="card-refresh" data-toggle="refresh"><i
							class="card-icon card-icon-refresh-lg-white"></i></a>
					</li>
				</ul>
			</div>
		</div>
		<div class="card-body">
			<div class="pull-bottom bottom-left bottom-right padding-25">
				<span class="label font-montserrat fs-11">NEWS</span>
				<br>

				<h3 class="text-white">So much more to see at a glance.</h3>

				<p class="text-white hint-text d-none d-lg-block d-xl-block d-none d-lg-block d-xl-block">Learn More at Project Pages</p>
			</div>
		</div>
	</div>