import { Routes } from '@angular/router';
// Layouts
import { CondensedComponent, RootLayout, BlankComponent } from './@pages/layouts';

import {DashboardComponent} from './dashboard/dashboard.component';
import {UsersComponent} from './users/users.component';
export const AppRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard/events',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: CondensedComponent,
    children: [{
      path: 'events',
      component: DashboardComponent
    }, {
      path: 'users',
      component: UsersComponent
    }, {
      path: 'forms',
      loadChildren: './forms/forms.module#FormsPageModule'
    }, {
      path: 'reports',
      loadChildren: './extra/extra.module#ExtraModule'
    }]
  },
  {
    path: '',
    component: BlankComponent,
    children: [{
      path: 'session',
      loadChildren: './session/session.module#SessionModule'
    }]
  }
];
