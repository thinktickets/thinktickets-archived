import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormsRoutes } from './forms.routing';
import { TabsModule } from 'ngx-bootstrap';
import { FormWizardComponent } from './form-wizard/form-wizard.component';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
import { SharedModule } from '../@pages/components/shared.module';

import { pgSelectModule} from '../@pages/components/select/select.module';
import { pgTagModule } from '../@pages/components/tag/tag.module';
import { pgSwitchModule } from '../@pages/components/switch/switch.module';
import { pgTimePickerModule } from '../@pages/components/time-picker/timepicker.module';
import { pgTabsModule } from '../@pages/components/tabs/tabs.module';
import { pgSelectFxModule } from '../@pages/components/selectfx/selectfx.module';
import { pgDatePickerModule } from '../@pages/components/datepicker/datepicker.module';
import { TypeaheadModule } from 'ngx-bootstrap';
import { TextMaskModule } from 'angular2-text-mask';

import { pgUploadModule } from '../@pages/components/upload/upload.module';
import { QuillModule } from 'ngx-quill';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(FormsRoutes),
    FormsModule,ReactiveFormsModule,
    TabsModule.forRoot(),
    pgSelectModule,
    pgTagModule,
    TextMaskModule,
    pgSwitchModule,
    pgTimePickerModule,
    pgTabsModule,
    pgSelectFxModule,
    pgUploadModule,
    TypeaheadModule.forRoot(),
    pgDatePickerModule,
    QuillModule
  ],
  declarations: [FormWizardComponent, FormElementsComponent, FormLayoutsComponent],
  providers: []
})
export class FormsPageModule { }
