import { Component, OnInit } from '@angular/core';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

@Component({
  selector: 'app-form-elements',
  templateUrl: './form-elements.component.html',
  styleUrls: ['./form-elements.component.scss']
})
export class FormElementsComponent implements OnInit {

  options = [
    { value: 'jack', label: 'Jack' },
    { value: 'lucy', label: 'Lucy' },
    { value: 'disabled', label: 'Disabled', disabled: true }
  ];
  tags = ["smith","jane"];
  selectedOption: any;

  searchOptions = [
    { value: 'jack', label: 'Jack' },
    { value: 'lucy', label: 'Lucy' },
    { value: 'tom', label: 'Tom' }
  ];
  selectedMultipleOption = [ 'Tom', 'Jack' ];
  
  states: string[] = [
    'Alabama',
    'Alaska',
    'Arizona',
    'Arkansas',
    'California',
    'Colorado',
    'Connecticut',
    'Delaware',
    'Florida',
    'Georgia',
    'Hawaii',
    'Idaho',
    'Illinois',
    'Indiana',
    'Iowa',
    'Kansas',
    'Kentucky',
    'Louisiana',
    'Maine',
    'Maryland',
    'Massachusetts',
    'Michigan',
    'Minnesota',
    'Mississippi',
    'Missouri',
    'Montana',
    'Nebraska',
    'Nevada',
    'New Hampshire',
    'New Jersey',
    'New Mexico',
    'New York',
    'North Dakota',
    'North Carolina',
    'Ohio',
    'Oklahoma',
    'Oregon',
    'Pennsylvania',
    'Rhode Island',
    'South Carolina',
    'South Dakota',
    'Tennessee',
    'Texas',
    'Utah',
    'Vermont',
    'Virginia',
    'Washington',
    'West Virginia',
    'Wisconsin',
    'Wyoming'
  ];
  _date = null;

  // Input Examples Masks
  mask = {
    date : [/[1-9]/, /\d/,'/', /\d/, /\d/,'/', /\d/, /\d/, /\d/, /\d/],
    telephone:['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/],
    custom:[/[1-9]/, /\d/,'-', /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/],
    ssn:[/[1-9]/, /\d/, /\d/,'-', /\d/, /\d/,'-', /\d/, /\d/, /\d/, /\d/],
  };
  numberMask = createNumberMask({
    prefix: '$ ',
    suffix: ''
  });
  wierdMask = createNumberMask({
    prefix: '',
    suffix: '',
    thousandsSeparatorSymbol:'.',
    allowDecimal:true,
    decimalSymbol:','
  });
  dollarPrefix = createNumberMask({
    prefix: '$ ',
    suffix: '',
    allowDecimal:true
  });
  range = createNumberMask({
    prefix: '',
    suffix: '',
    integerLimit:4
  });
  ngOnInit() {

  }
}
