import { Routes } from '@angular/router';
import { FormWizardComponent } from './form-wizard/form-wizard.component';
import { FormElementsComponent } from './form-elements/form-elements.component';
import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
export const FormsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'basic',
      component: FormElementsComponent
    }, {
      path: 'layouts',
      component: FormLayoutsComponent
    }, {
      path: 'wizard',
      component: FormWizardComponent
    }]
  }
];
