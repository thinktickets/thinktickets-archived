import { Component, OnInit, ViewChild} from '@angular/core';
import { pgCollapseComponent } from '../@pages/components/collapse';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { DashboardService } from './dashboard.service';
import * as mandrill from 'mandrill-api/mandrill';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-tabspage',
  templateUrl: './dashboard.html'
})

export class DashboardComponent implements OnInit {

  m = new mandrill.Mandrill('jMvK-ZMEdMtawF9MOt3GvQ');

  events: Observable<any[]>;
  users: Observable<any[]>;

  tours = [];
  tour = 0;
  event = 0;
  tourId = '0';
  eventId = '0';
  user = {
    id: 'GiantDwarf',
    name: 'Giant Dwarf',
    email: 'tickets@giantdwarf.com.au',
    logo: 'https://giantdwarf.com.au/wp-content/uploads/2018/03/giantdwarf_head_logo_new_2@2x.png',
    address: '199 Cleveland St, Redfern NSW, Australia',
    domain: 'giantdwarf.com.au'
  };

  basicRows = [];
  basicSort = [];
  columns = [
    { name: 'Name'},
    { name: 'Email' },
    { name: 'Address'},
    { name: 'Number' },
    { name: 'Seats'}
  ];
  purchases = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private _service: DashboardService, private db: AngularFirestore) {

    this.events = db.collection('events', ref => ref.where('admin.' + this.user.id, '==', 'true')).valueChanges();
    // this.users = db.collection('users', ref => ref.where('events.' + this.eventId + '.viewed', '>', 0)).valueChanges();

    this.events.subscribe(data => {
      for (const i in data[0].options) {
        if (i) {
          this.purchases.push({name: data[0].options[i][0].name, prop: data[0].options[i][0].name});
        }
      }
      this.tours = this.groupBy(data, 'tour');
      this.tourId = Object.keys(this.tours)[this.tour];
      this.eventId = this.tours[this.tourId]['events'][this.event].id;
      this.getUsers();
    });
  }

  getUsers() {
    this.users = this.db.collection('users', ref => ref.where('events.' + this.eventId + '.purchased', '>', '0')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
    for (const j in this.tours[this.tourId].events[this.event].options) {
      if (j) {
        this.tours[this.tourId].events[this.event].options[j][0].sold = 0;
      }
    }
    this.users.subscribe(data => {
      data.forEach(user => {
        user.name = user.fName + ' ' + user.lName;
        user.address = user.address + ' ' + user.city + ', ' + user.state + ' ' + user.postcode;
        user.seats = [];
        for (const i in user.events[this.eventId].purchases) {
          if (i) {
            user.seats.push(user.events[this.eventId].purchases[i].reserve)
            this.tours[this.tourId].events[this.event].options[user.events[this.eventId].purchases[i].reserve][0].sold ++;
          }
        }
      });
      this.basicSort = [...data];
      this.basicRows = data;
    });
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  tabChange(index, event) {
    this.eventId = event;
    this.event = index;
    this.getUsers();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    if (val[0] === ':') {
      const query = val.split(':');
      if (query.length === 3) {
          const temp = this.basicSort.filter(function(d) {
            return d[query[1]].toLowerCase().indexOf(query[2]) !== -1 || !query[2];
          });
          this.basicRows = temp;
          this.table.offset = 0;
      }
    } else {
      // filter our data
    const temp = this.basicSort.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.basicRows = temp;
    this.table.offset = 0;
    }
  }

  groupBy(arr, property) {
    return arr.reduce(function(memo, x) {
      for (let i in x.options) {
        if (i) {
          x.options[i][0].sold = 0;
        }
      }
      if (!memo[x[property]]) { memo[x[property]] = {events: [], total: 0}; }
      // memo[x[property]]['event'].push('hi');
      memo[x[property]]['events'].push(x);
      memo[x[property]]['total'] += x.total;
      // memo[x[property]]event.push(x);
      return memo;
    }, {});
  }

  sendMessage(address, user, eventName) {
    if (!address) {
      address = user.email;
    }
    this.m.messages.send({
      'message': {
        'from_email': this.user.email,
        'from_name': this.user.name,
        'to': [{'email': address, 'name': user.name}],
        'subject': 'Ticket: ' + eventName,
        'html': document.querySelector('#receipt').outerHTML
      }
    });
  }

  ngOnInit() {
  }

}
