import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ProgressModule } from './@pages/components/progress/progress.module';


import { AppRoutes } from './app.routing';
import { AppComponent } from './app.component';

// Layouts
import { CondensedComponent } from './@pages/layouts';
import { RootLayout } from './@pages/layouts';
import { BlankComponent } from './@pages/layouts';
import { pagesToggleService } from './@pages/services/toggler.service';

// Shared Components
import { SidebarComponent } from './@pages/components/sidebar/sidebar.component';
import { QuickviewComponent } from './@pages/components/quickview/quickview.component';
import { QuickviewService } from './@pages/components/quickview/quickview.service';
import { DashboardService } from './dashboard/dashboard.service';
import { SearchOverlayComponent } from './@pages/components/search-overlay/search-overlay.component';
import { HeaderComponent } from './@pages/components/header/header.component';
import { SharedModule } from './@pages/components/shared.module';
import { pgCardModule} from './@pages/components/card/card.module';

// Basic Bootstrap Modules
import { BsDropdownModule } from 'ngx-bootstrap';
import { AccordionModule } from 'ngx-bootstrap';
import { AlertModule } from 'ngx-bootstrap';
import { ButtonsModule } from 'ngx-bootstrap';
import { CollapseModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';
import { ProgressbarModule } from 'ngx-bootstrap';
import { TabsModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap';
import { FormWizardComponent } from './forms/form-wizard/form-wizard.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ReceiptComponent } from './receipt/receipt.component';
import { UsersComponent } from './users/users.component';
import { TypeaheadModule } from 'ngx-bootstrap';
import { pgTabsModule } from './@pages/components/tabs/tabs.module';
import { FeatherIconsModule } from './@pages/modules/feather-icons/feather-icons.module';

import { NvD3Module } from 'ngx-nvd3';
import { NgxEchartsModule } from 'ngx-echarts';
import { RickshawModule } from 'ng2-rickshaw';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import {NgxDnDModule} from '@swimlane/ngx-dnd';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

// PIPES
import {GetInitialsPipe} from './@pages/global.pipe';
import {MapToIterable} from './@pages/global.pipe';

// FIRESTORE
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    AppComponent,
    CondensedComponent, RootLayout, BlankComponent,
    SidebarComponent, QuickviewComponent, SearchOverlayComponent, HeaderComponent,
    DashboardComponent,
    UsersComponent,
    ReceiptComponent,
    GetInitialsPipe,
    MapToIterable
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'perth-d7115'),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFirestoreModule.enablePersistence(),
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    SharedModule,
    ProgressModule,
    pgCardModule,
    RouterModule.forRoot(AppRoutes),
    BsDropdownModule.forRoot(),
    AccordionModule.forRoot(),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
    CollapseModule.forRoot(),
    ModalModule.forRoot(),
    ProgressbarModule.forRoot(),
    TabsModule.forRoot(),
    TooltipModule.forRoot(),
    TypeaheadModule.forRoot(),
    FeatherIconsModule,
    NvD3Module,
    pgTabsModule,
    NgxEchartsModule,
    RickshawModule,
    NgxDnDModule,
    PerfectScrollbarModule,
    NgxDatatableModule
  ],
  providers: [QuickviewService, pagesToggleService, DashboardService, {
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent],
  exports: [GetInitialsPipe]
})
export class AppModule { }
