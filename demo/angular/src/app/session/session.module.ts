import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { SessionRoute } from './session.routing';

import { SharedModule } from '../@pages/components/shared.module';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(SessionRoute),
  ],
  declarations: [ErrorComponent,LoginComponent,LockscreenComponent]
})
export class SessionModule { }
