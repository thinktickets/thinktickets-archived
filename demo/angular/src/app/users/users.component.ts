import { Component, OnInit, ViewChild} from '@angular/core';
import { pgCollapseComponent } from '../@pages/components/collapse';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import { ModalDirective } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-tabspage',
  templateUrl: './users.html'
})

export class UsersComponent implements OnInit {
  events: Observable<any[]>;
  users: Observable<any[]>;

  user = {
    id: 'info',
    name: 'Giant Dwarf',
    email: 'tickets@giantdwarf.com.au',
    logo: 'https://giantdwarf.com.au/wp-content/uploads/2018/03/giantdwarf_head_logo_new_2@2x.png',
    address: '199 Cleveland St, Redfern NSW, Australia',
    domain: 'giantdwarf.com.au'
  };

  basicRows = [];
  basicSort = [];
  columns = [
    { name: 'Name'},
    { name: 'Email' },
    { name: 'Address'},
    { name: 'Number' },
    { name: 'Attended'}
  ];
  purchases = [];

  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private db: AngularFirestore) {
    this.getUsers();
  }

  getUsers() {
    this.users = this.db.collection('users').snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data();
        const id = a.payload.doc.id;
        return { id, ...data };
      })));
    this.users.subscribe(data => {
      data.forEach(user => {
        user.name = user.fName + ' ' + user.lName;
        user.address = user.address + ' ' + user.city + ', ' + user.state + ' ' + user.postcode;
        user.attended = user.events ? Object.keys(user.events).length : '';
      });
      this.basicSort = [...data];
      this.basicRows = data;
    });
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  log(index, event) {
    this.getUsers();
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    if (val[0] === ':') {
      const query = val.split(':');
      if (query.length === 3) {
          const temp = this.basicSort.filter(function(d) {
            return d[query[1]].toLowerCase().indexOf(query[2]) !== -1 || !query[2];
          });
          this.basicRows = temp;
          this.table.offset = 0;
      }
    } else {
      // filter our data
    const temp = this.basicSort.filter(function(d) {
      return d.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    this.basicRows = temp;
    this.table.offset = 0;
    }
  }

  ngOnInit() {
  }

}
