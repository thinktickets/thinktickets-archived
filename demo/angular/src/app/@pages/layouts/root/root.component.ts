import { Component, OnInit,ViewChild, Input } from '@angular/core';
import { pagesToggleService } from '../../services/toggler.service';
declare var pg: any;
@Component({
  selector: 'root-layout',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.scss'],
})
export class RootLayout implements OnInit {

  @ViewChild('root') root;
  layoutState:string;
  _menuPin:boolean = false;

  @Input()
  public contentClass:string = "";
 
  @Input()
  public pageWrapperClass:string = "";  

  @Input()
  public footer:boolean = true;

  constructor(private toggler:pagesToggleService) { 
  	if(this.layoutState){
  		pg.addClass(document.body,this.layoutState);
  	}
  }

  changeLayout(type:string){
  	pg.removeClass(document.body,this.layoutState);
  	pg.addClass(document.body,type);
  }
  removeLayout(type:string){
  	pg.removeClass(document.body,type);
  }

  ngOnInit() {
  }

  openQuickView($e){
		$e.preventDefault();
		this.toggler.toggleQuickView();
	}
	openSearch($e){
		$e.preventDefault();
		this.toggler.toggleSearch(true);
  }
  toggleMenuPin($e){
    if(this._menuPin){
      pg.removeClass(document.body,"menu-pin");
      this._menuPin = false;
    }else{
      pg.addClass(document.body,"menu-pin");
      this._menuPin = true;
    }
  }

}
