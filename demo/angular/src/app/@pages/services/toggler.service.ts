import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class pagesToggleService {
  private _searchToggle = new Subject();
  searchToggle = this._searchToggle.asObservable();

  private _quickViewToggle = new Subject();
  quickViewToggle = this._quickViewToggle.asObservable();

  private _sideBarToggle = new Subject();
  sideBarToggle = this._sideBarToggle.asObservable();


  private _menuPinToggle = new Subject();
  menuPinToggle = this._menuPinToggle.asObservable();

  toggleSearch(toggle:boolean) {
    this._searchToggle.next({text:toggle});
  }

  toggleMenuPin(toggle:boolean){
    this._menuPinToggle.next({text:toggle});
  }
  
  toggleQuickView() {
    this._quickViewToggle.next();
  }

  toggleMobileSideBar(){
    this._sideBarToggle.next();
  }

}