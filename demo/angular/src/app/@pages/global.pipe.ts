import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'getInitials'
})

export class GetInitialsPipe implements PipeTransform {
  transform(value: string) {
    return value.replace(/[^a-zA-Z- ]/g, '').match(/\b\w/g).join('');
  }
}

@Pipe({
  name: 'mapToIterable'
})

export class MapToIterable implements PipeTransform {
  transform(dict: Object) {
  let a = [];
    for (let key in dict) {
      if (dict.hasOwnProperty(key)) {
        a.push({key: key, val: dict[key]});
      }
    }
    return a;
  }
}
