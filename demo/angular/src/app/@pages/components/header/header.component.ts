import { Component, OnInit,Inject,forwardRef,Input } from '@angular/core';

@Component({
  selector: 'pg-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	@Input()
	boxed:boolean = false;

	constructor() { }

	ngOnInit() {
	}

}
