import { Component, OnInit,ElementRef,ViewEncapsulation, Inject, forwardRef, Input,ViewChild,TemplateRef,ContentChild, } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { pagesToggleService} from '../../services/toggler.service';
declare var pg: any;


@Component({
  selector: 'pg-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  host: {'class': 'page-sidebar'},
  encapsulation: ViewEncapsulation.None
})
export class SidebarComponent implements OnInit {
  subscription: Subscription;
  pin:boolean = false;
  sidebar;
  @ContentChild('sideBarOverlay') sideBarOverlay: TemplateRef<void>;
  @ContentChild('sideBarHeader') sideBarHeader: TemplateRef<void>;
  @ContentChild('menuItems') menuItems: TemplateRef<void>;
  constructor(private appSidebar: ElementRef,private toggler:pagesToggleService) { 
  	this.subscription = this.toggler.sideBarToggle.subscribe(message => { this.toggleMobile() });
  }

  ngOnInit() {
    this.sidebar = new pg.SideBar(this.appSidebar.nativeElement);
  }

  toggleMenuPin(){
    if(this.pin){
      this.pin = false;
      //this.layout.removeLayout("menu-pin");
    }
    else{
      this.pin = true;
      //this.layout.changeLayout("menu-pin");
    }
  }
  toggleMobile(){
    this.sidebar.toggleSidebar();
  }
}
