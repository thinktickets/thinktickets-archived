import {
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { pgCollapseComponent } from './collapse.component';

@Component({
  selector     : 'pg-collapseset',
  encapsulation: ViewEncapsulation.None,
  template     : `
    <div class="card-group" [class.horizontal]="horizontal">
      <ng-content></ng-content>
    </div>
  `,
})
export class pgCollapsesetComponent {
  private _accordion = false;
  private _horizontal = true;
  panels: pgCollapseComponent[] = [];

  @Input()
  set pgAccordion(value: boolean) {
    this._accordion = value;
  }

  get pgAccordion(): boolean {
    return this._accordion;
  }

  @Input()
  set horizontal(value: boolean) {
    this._horizontal = value;
  }

  get horizontal(): boolean {
    return this._horizontal;
  }

  pgClick(collapse: pgCollapseComponent): void {
    if (this.pgAccordion) {
      this.panels.map((item, index) => {
        const curIndex = this.panels.indexOf(collapse);
        if (index !== curIndex) {
          item.pgActive = false;
        }
      });
    }
  }

  addTab(collapse: pgCollapseComponent): void {
    this.panels.push(collapse);
  }
}
