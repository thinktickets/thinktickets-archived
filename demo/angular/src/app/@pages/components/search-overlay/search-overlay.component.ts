import { Component, OnInit, OnDestroy,HostBinding,ElementRef,Input,HostListener } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { fadeAnimation } from '../../animations/fade-animations';
import { pagesToggleService} from '../../services/toggler.service';
import { SearchResult } from './search-result';
@Component({
  selector: 'app-search-overlay',
  templateUrl: './search-overlay.component.html',
  animations   : [
    fadeAnimation
  ],
  styleUrls: ['./search-overlay.component.scss']
})
export class SearchOverlayComponent implements OnDestroy {
  private toggleSubscription : Subscription;
  private _isEnabled:boolean = false;
  public isVisible:boolean = false;
  public value:string = "";
  private modal:SearchResult;

  constructor(private el: ElementRef,private toggler:pagesToggleService) {
    this.toggleSubscription = this.toggler.searchToggle.subscribe((toggleValue) => { this.open() });
  }
  ngOnDestroy() {
    this.toggleSubscription.unsubscribe();
  }

  @Input() set isEnabled(isEnabled: boolean) {
    this.isEnabled = isEnabled;
  }
  get isEnabled() {
    return this._isEnabled;
  }

  close($event){
    $event.preventDefault();
  	this.isVisible = false;
  	this.value = "";
  }

  open(){
    this.isVisible = true;
    this.value = "";
  }

  @HostListener('document:keyup', ['$event']) onKeydownHandler(event: KeyboardEvent) {
  	if(!this._isEnabled){
  		return;
  	}
  	if (this.isVisible && event.keyCode == 27) {
  		this.isVisible = false;
  		this.value = "";
  	}

 //  	var nodeName = event.target.nodeName;
	// if (nodeName == 'INPUT' ||
	//     nodeName == 'TEXTAREA') {
	//     return;
	// }

  	
  	//console.log(event)
    if (event.which !== 0 && !event.ctrlKey && !event.metaKey && !event.altKey && event.keyCode != 27) {
    	this.isVisible = true;
    	if(!this.value)
        	this.value = String.fromCharCode(event.keyCode | event.charCode);
        else
        	this.value = this.value + String.fromCharCode(event.keyCode | event.charCode);
    }
  }
}
