import { Directive,ElementRef,HostListener } from '@angular/core';

@Directive({
  selector: '[pgFormGroupDefault]'
})
export class FormGroupDefaultDirective {
 		
	constructor(private El: ElementRef) { 
		
	}

	@HostListener('focus') onFocus() {
    	this.El.nativeElement.parentNode.classList.add('focused');
	}

	@HostListener('focusout') onFocusOut() {
    	this.El.nativeElement.parentNode.classList.remove('focused');
	}
}
