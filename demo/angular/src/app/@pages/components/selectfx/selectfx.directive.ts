import { Directive, ElementRef, Renderer2, Input, HostListener } from '@angular/core';
declare var pg: any;

@Directive({
  selector: '[pg-selectfx]'
})
export class SelectfxDirective {

 	constructor(private select: ElementRef,private renderer: Renderer2) { 
   }
   // Will not support model change
   // @TODO : Create event change and update.
   ngOnInit() {
		this.select = new pg.Select(this.select.nativeElement)
	}

}
