import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SelectfxDirective } from './selectfx.directive';

@NgModule({
  exports     : [ SelectfxDirective ],
  declarations: [ SelectfxDirective ],
  imports     : [ CommonModule ]
})
export class pgSelectFxModule {
}
