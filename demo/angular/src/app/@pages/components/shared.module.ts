import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpModule} from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { QuickviewService} from './quickview/quickview.service';
import { SidebarComponent } from './sidebar/sidebar.component';
import { QuickviewComponent } from './quickview/quickview.component';
import { QuickviewToggleDirective } from './quickview/quickview-toggle.directive';
import { SearchOverlayComponent } from './search-overlay/search-overlay.component';
import { HeaderComponent } from './header/header.component';

import { TypeaheadModule } from 'ngx-bootstrap';

import { ParallaxDirective } from './parallax/parallax.directive';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';
import { FormGroupDefaultDirective } from './forms/form-group-default.directive';
import { ViewDirective } from './view/view.directive';

import { pgCollapseModule } from './collapse/collapse.module';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  imports: [
    CommonModule,
    TypeaheadModule.forRoot(),
    PerfectScrollbarModule
  ],
  declarations: [
  ParallaxDirective,
  BreadcrumbComponent,
  FormGroupDefaultDirective,
  ViewDirective,
  QuickviewToggleDirective
  ],
  exports: [
  ParallaxDirective,
  BreadcrumbComponent,
  FormGroupDefaultDirective,
  ViewDirective,
  pgCollapseModule,
  ],
  providers: [QuickviewService,
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]

})
export class SharedModule { }
