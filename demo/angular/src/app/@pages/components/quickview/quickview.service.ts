import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class QuickviewService {

  constructor(private http: Http) { }

  httpOptions = {
    headers: new Headers({
      'Content-Type':  'application/json',
      'Authorization': 'Basic ' + btoa('fWnNcPLIjPPChYzybgkv')
    })
  };

  // Get all emails from the API
  getNotes() {
    return this.http.get('assets/data/notes.json')
      .map(res => res.json());
  }

  getUsers() {
    /* const headers = {
      Authorization: 'Basic ' + btoa('fWnNcPLIjPPChYzybgkv')
    }; */
    return this.http.get('https://perth18.freshdesk.com/api/v2/tickets/?include=requester', this.httpOptions).map(
      res => [res.json().filter(ticket => ticket.status === 2), res.json().filter(ticket => ticket.status !== 2)]
    );
  }

  getConversations(id) {
    /* const headers = {
      Authorization: 'Basic ' + btoa('fWnNcPLIjPPChYzybgkv')
    }; */
    return this.http.get('https://perth18.freshdesk.com/api/v2/tickets/' + id + '/conversations', this.httpOptions).map(
      res => res.json()
    );
  }

  sendConversations(reply, id) {
    /* const headers = {
      Authorization: 'Basic ' + btoa('fWnNcPLIjPPChYzybgkv')
    }; */
    return this.http.post('https://perth18.freshdesk.com/api/v2/tickets/' + id + '/reply', {
      'body': reply
    }, this.httpOptions).map(
      res => res.json()
    );
  }

}
