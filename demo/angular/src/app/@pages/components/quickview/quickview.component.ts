import {
  Component,
  OnInit,
  OnDestroy,
  ViewEncapsulation,
  Output,
  EventEmitter
} from '@angular/core';
import {
  Subscription
} from 'rxjs/Subscription';
import {
  HttpClient
} from '@angular/common/http';
import * as moment from 'moment';
import {
  pagesToggleService
} from '../../services/toggler.service';
import {
  QuickviewService
} from './quickview.service';
import {
  Note
} from './note';

@Component({
  selector: 'app-quickview',
  templateUrl: './quickview.component.html',
  styleUrls: ['./quickview.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class QuickviewComponent implements OnInit, OnDestroy {
  @Output() openIssues = new EventEmitter<boolean>();
  subscription: Subscription;
  isOpen: boolean = false;
  openNumber = 0;
  noteList = [];
  // Single
  selectedNote: Note;
  // List for deleting or CRUD functions
  deleteNoteMode: boolean = false;
  isNoteOpen = false;
  userList = [];
  conversation = [];
  constructor(private _service: QuickviewService, private http: HttpClient, private toggler: pagesToggleService) {
    this.subscription = this.toggler.quickViewToggle.subscribe(message => {
      this.toggle()
    });
  }
  
  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }
  ngOnInit() {
    this._service.getUsers().subscribe(users => {
      this.userList = users;
      if (this.userList[0].length > 0) {
        this.openIssues.emit(true);
        this.openNumber = this.userList[0].length;
      }
    });
  }
  toggle() {
    if (this.isOpen) {
      this.isOpen = false;
    } else {
      this.isOpen = true;
    }
  }

  getUsers() {
    this._service.getUsers().subscribe(users => {
      this.userList = users;
      if (this.userList[0].length > 0) {
        this.openIssues.emit(true);
        this.openNumber = this.userList[0].length;
      }
    });
  }

  getConversation(ticket) {
   this.conversation = [ticket];
    this._service.getConversations(ticket.id).subscribe(conversations => {
      this.conversation = [ticket].concat( conversations);
      console.log(this.conversation);
    });
  }

  sendConversation(item, id) {
    this.conversation.push({
      'body': item,
      incoming: false
    });
    this._service.sendConversations(item, id).subscribe(conversation => {
      console.log(conversation);
    });
  }

  popNote(item: Note): void {
    const index = this.noteList.indexOf(item);
    if (index !== -1) {
      this.noteList.splice(index, 1);
    }
  }

  pushNote(item: Note): void {
    this.noteList.push(item);
  }

  onSelectNote(item: Note): void {
    if (!this.deleteNoteMode) {
      this.selectedNote = item;
      this.isNoteOpen = true;
    }

  }
  toggleNotesView(): void {
    if (this.isNoteOpen) {
      this.isNoteOpen = false;
    } else {
      this.isNoteOpen = true;
    }
  }

  onCheck(e, item: Note): void {
    if (e.target.checked) {
      this.pushNote(item);
    } else {
      this.popNote(item);
    }
  }

  composeNote(): void {
    this.isNoteOpen = true;
    this.selectedNote = new Note;
    this.selectedNote.id = this.noteList.length + 1;
    this.selectedNote.date = new Date();
    this.pushNote(this.selectedNote);
  }

  saveNote(e): void {
    this.selectedNote.notes = e.target.value;
    // this.noteList[this.selectedNote[]]
  }

  deleteMode(): void {
    if (this.deleteNoteMode)
      this.deleteNoteMode = false;
    else
      this.deleteNoteMode = true;
  }

  deleteNote(): void {
    this.noteList = this.noteList.filter(item => this.noteList.indexOf(item) < 0);
  }


}

