import { Directive, Input, Output, EventEmitter} from '@angular/core';
declare var pg: any;
@Directive({
  selector: '[pg-quick-view-toggle]',
  host: {
    '[attr.role]': 'role',
    '(click)': 'toggleState()'
  }
})
export class QuickviewToggleDirective {
  constructor() { }
  role = 'button';
  toggleState() {
    // TODO:Change
    const qv = document.getElementById('quickview');
    pg.toggleClass(qv, 'open');
  }
}
