import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Import required Icons
import { 
  IconShield,
  IconMail,
  IconUsers,
  IconCalendar,
  IconCpu,
  IconLayout, 
  IconTriangle, 
  IconList, 
  IconGrid, 
  IconAirplay, 
  IconSquare, 
  IconMapPin, 
  IconBarChart, 
  IconBox, 
  IconMenu, 
  IconLifeBuoy,
  IconMessageCircle,
  IconMessageSquare,
  IconLink,
  IconPieChart,
  IconClipboard,
  IconChevronDown,
  IconChevronUp} from 'angular-feather';

  //Temporary List
  const exportIcons = [
    IconShield,
    IconMail,
    IconUsers,
    IconCalendar,
    IconCpu,
    IconLayout, 
    IconTriangle, 
    IconList, 
    IconGrid, 
    IconAirplay, 
    IconSquare, 
    IconMapPin, 
    IconBarChart, 
    IconBox, 
    IconMenu, 
    IconLifeBuoy,
    IconMessageCircle,
    IconMessageSquare,
    IconLink,
    IconPieChart,
    IconClipboard,
    IconChevronDown,
    IconChevronUp
  ];

@NgModule({
  imports: [
    CommonModule
  ],
  exports:[ 
    exportIcons
  ],
  declarations: []
})
export class FeatherIconsModule { }
