import { Component, OnInit, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.html',
  styleUrls: ['./receipt.component.scss']
})
export class ReceiptComponent implements OnInit {
  @Input() event: any = {};
  @Input() user: any = {};
  @Input() promoter: any = {};
  imgMap = '';
  date: Date;

  ngOnInit() {
    this.date = new Date();
    this.imgMap = 'https://maps.googleapis.com/maps/api/staticmap?center=' +
    this.event.address +
    '&zoom=15&size=600x230&maptype=roadmap&scale=2&markers=size:large%7Ccolor:0x2f2f2f%7Clabel:1%7C' +
    this.event.address +
    '&key=AIzaSyDckJReG6nDmI0zkErZAO0Gq58g9T7cLYY';
  }
}

