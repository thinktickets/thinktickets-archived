import { Routes } from '@angular/router';
import { BlankpageComponent } from './blankpage/blankpage.component';
import { TimelineComponent } from './timeline/timeline.component';
import { InvoiceComponent } from './invoice/invoice.component';
export const ExtraRouts: Routes = [
  {
    path: '',
    children: [{
      path: 'blank',
      component: BlankpageComponent
    },{
      path: 'timeline',
      component: TimelineComponent
    },{
      path: 'invoice',
      component: InvoiceComponent
    }]
  }
];
