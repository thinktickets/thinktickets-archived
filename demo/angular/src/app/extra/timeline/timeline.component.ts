import { Component, OnInit } from '@angular/core';
import {NgsRevealConfig} from 'ng-scrollreveal';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {

   constructor(config: NgsRevealConfig) {
    config.distance = "0";
  }

  ngOnInit() {
  }

}
