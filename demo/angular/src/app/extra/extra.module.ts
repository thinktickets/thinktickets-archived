import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {NgsRevealModule} from 'ng-scrollreveal';
import { ExtraRouts } from './extra.routing';
import { BlankpageComponent } from './blankpage/blankpage.component';
import { SharedModule } from '../@pages/components/shared.module';
import { GalleryComponent } from './gallery/gallery.component';
import { TimelineComponent } from './timeline/timeline.component';
import { InvoiceComponent } from './invoice/invoice.component';
@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ExtraRouts),
    NgsRevealModule.forRoot()
  ],
  declarations: [BlankpageComponent, GalleryComponent, TimelineComponent, InvoiceComponent]
})
export class ExtraModule { }
